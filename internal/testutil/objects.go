// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package testutil

import (
	"encoding"
	"fmt"
	"strings"
	"testing"

	"gitlab.com/zygoon/go-diff"
)

// Equatable is the interface that wraps the Equal method.
type Equatable[T any] interface {
	Equal(T) bool
}

func renderSomeText[T any](v T) (string, error) {
	if vMarshaler, ok := any(v).(encoding.TextMarshaler); ok {
		blob, err := vMarshaler.MarshalText()
		if err != nil {
			return "", err
		}

		return string(blob), nil
	}

	return fmt.Sprintf("%#v", v), nil
}

// ObjectEqual ensures that two objects compare equal with the Equal method.
func ObjectEqual[T Equatable[T]](t *testing.T, a, b T) {
	t.Helper()

	if !a.Equal(b) {
		aText, err := renderSomeText(a)
		if err != nil {
			t.Errorf("Cannot render text of a: %v", err)
		}

		bText, err := renderSomeText(b)
		if err != nil {
			t.Errorf("Cannot render text of b: %v", err)
		}

		if strings.ContainsRune(aText, '\n') {
			t.Logf("Textual representation of a: (starts on the following line)\n%s", aText)
		} else {
			t.Logf("Textual representation of a: %s", aText)
		}

		if strings.ContainsRune(bText, '\n') {
			t.Logf("Textual representation of b: (starts on the following line)\n%s", bText)
		} else {
			t.Logf("Textual representation of b: %s", bText)
		}

		if aText != bText {
			diffText := diff.Unified("a", "b", aText, bText)
			t.Logf("Difference in textual representations:\n%s", diffText)
		} else {
			t.Logf("Textual representation of both a and b is identical")
		}

		t.Error("The values a and b are not equal")
	}
}

// ObjectSliceEqual ensures that two slices have the same length and that their elements compare equal with the Equal method.
func ObjectSliceEqual[T Equatable[T]](t *testing.T, a, b []T) {
	t.Helper()

	slicesEqualLength(t, a, b)

	for i := range a {
		if !a[i].Equal(b[i]) {
			slicesDifferentError(t, i, a, b)
		}
	}
}

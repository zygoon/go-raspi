// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package testutil

import "testing"

// SliceHasLen ensures that the argument has given length.
func SliceHasLen[T any](t *testing.T, v []T, length int) {
	t.Helper()

	if l := len(v); l != length {
		t.Fatalf("Unexpected slice length: %v", l)
	}
}

// SlicesEqual ensures that two slices have the same length and that their elements compare equal.
func SlicesEqual[T comparable](t *testing.T, a, b []T) {
	t.Helper()

	slicesEqualLength(t, a, b)

	for i := range a {
		if a[i] != b[i] {
			slicesDifferentError(t, i, a, b)
		}
	}
}

func slicesEqualLength[T any](t *testing.T, a, b []T) {
	t.Helper()

	if len(a) != len(b) {
		t.Fatalf("Slices have different length %d != %d", len(a), len(b))
	}
}

func slicesDifferentError[T any](t *testing.T, i int, a, b []T) {
	t.Errorf("Slices differ at index %d: %v != %v", i, a[i], b[i])
}

// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package pieeprom_test

import (
	"os"
	"path/filepath"
	"testing"
	"time"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/pieeprom"
)

func TestBuildTimestamp(t *testing.T) {
	d := t.TempDir()
	n := filepath.Join(d, "build-timestamp")
	ts, err := pieeprom.BuildTimestamp(n)

	// This is what happens on devices without the EEPROM-based firmware.
	testutil.NotOk(t, err)
	testutil.ErrorIs(t, err, os.ErrNotExist)
	testutil.Equal(t, ts.IsZero(), true)

	// Data obtained from a system with firmware programmed with pieeprom-2021-04-29.bin.
	err = os.WriteFile(n, []byte{0x60, 0x8a, 0xda, 0xad}, 0o600)
	testutil.Ok(t, err)

	ts, err = pieeprom.BuildTimestamp(n)
	testutil.Ok(t, err)
	testutil.Equal(t, ts, time.Date(2021, time.April, 29, 16, 11, 25, 0, time.UTC))
}

func TestTryBootFeatureSince(t *testing.T) {
	testutil.Equal(t, pieeprom.TryBootFeatureSince, time.Date(2020, time.December, 11, 11, 15, 17, 0, time.UTC))
}

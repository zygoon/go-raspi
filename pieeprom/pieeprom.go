// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package pieeprom provides interface ot the Raspberry Pi bootloader EEPROM.
package pieeprom

import (
	"encoding/binary"
	"os"
	"time"
)

// ProcfsBootloaderBuildTimestamp denotes a device tree file containing the build timestamp of the firmware.
const ProcfsBootloaderBuildTimestamp = "/proc/device-tree/chosen/bootloader/build-timestamp"

// BuildTimestamp returns the build date of the firmware as read from the given file.
//
// The format of the timestamp is a 32bit big-endian signed integer representing
// seconds since UNIX epoch. Pass ProcfsBootloaderBuildTimestamp as the filename
// to load the timestamp from the device tree of a running system.
//
// On a device without EEPROM-based bootloader, this will fail with
// fs.ErrNotExist.
func BuildTimestamp(name string) (time.Time, error) {
	var zeroTime time.Time

	f, err := os.Open(name)
	if err != nil {
		return zeroTime, err
	}

	defer func() {
		_ = f.Close()
	}()

	var sec int32

	if err := binary.Read(f, binary.BigEndian, &sec); err != nil {
		return zeroTime, err
	}

	return time.Unix(int64(sec), 0).UTC(), nil
}

var (
	// TryBootFeatureSince is the initial build timestamp since the tryboot.txt feature is supported.
	TryBootFeatureSince = time.Date(2020, time.December, 11, 11, 15, 17, 0, time.UTC)
)

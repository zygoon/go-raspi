// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package pimodel contains identifiers of individual Raspberry Pi variants
// as well as logic to find the model of the running system.
package pimodel

import (
	"fmt"
)

// Model represents the model of the Raspberry Pi board.
//
// Variations with regard to storage and memory. Some models may come with
// varying type of SOC, this is also not represented.
type Model int

const (
	// NotPi represents device other than a Raspberry Pi
	NotPi Model = iota + 1
	// Pi0 represents Raspberry Pi Zero.
	Pi0
	// Pi0W represents Raspberry Pi Zero W (wireless).
	Pi0W
	// Pi02W represents Raspberry Pi Zero 2 W.
	Pi02W
	// Pi1A represents Raspberry Pi 1A.
	Pi1A
	// Pi1APlus represents Raspberry Pi 1A+.
	Pi1APlus
	// Pi1B represents Raspberry Pi 1B.
	Pi1B
	// Pi1BPlus represents Raspberry Pi 1B+.
	Pi1BPlus
	// Cm1 represents Raspberry Pi Compute Module (based on Pi1).
	Cm1
	// Pi2B represents Raspberry Pi 2B.
	Pi2B
	// PiAlpha represents Raspberry Pi Alpha (early prototype).
	PiAlpha
	// Pi3B represents Raspberry Pi 3B.
	Pi3B
	// Pi3APlus represents Raspberry Pi 3A+.
	Pi3APlus
	// Pi3BPlus represents Raspberry Pi 3B+.
	Pi3BPlus
	// Cm3 represents Raspberry Pi Compute Module 3.
	Cm3
	// Cm3Plus represents Raspberry Pi Compute Module 3+.
	Cm3Plus
	// Pi4B represents Raspberry Pi 4B.
	Pi4B
	// Pi400 represents Raspberry Pi 400.
	Pi400
	// Cm4 represents Raspberry Pi Compute Module 4.
	Cm4
	// Cm4S represents Raspberry Pi Compute Module 4 ("S" variant).
	Cm4S
	// Pi5 represents Raspberry Pi 5.
	Pi5
)

// Names are exactly like those reported by the firmware.
const (
	rPi        = "Raspberry Pi"
	rPiZero    = rPi + " Zero"
	rPiZeroW   = rPi + " Zero W"
	rPiZero2W  = rPi + " Zero 2 W"
	rPi1A      = rPi + " Model A"
	rPi1APlus  = rPi + " Model A Plus"
	rPi1B      = rPi + " Model B"
	rPi1BPlus  = rPi + " Model B Plus"
	rPiCm1     = rPi + " Compute Module"
	rPiAlpha   = rPi + " Alpha"
	rPi2B      = rPi + " 2 Model B"
	rPi3B      = rPi + " 3 Model B"
	rPi3APlus  = rPi + " 3 Model A Plus"
	rPi3BPlus  = rPi + " 3 Model B Plus"
	rPiCm3     = rPi + " Compute Module 3"
	rPiCm3Plus = rPi + " Compute Module 3 Plus"
	rPi4B      = rPi + " 4 Model B"
	rPi400     = rPi + " 400"
	rPiCm4     = rPi + " Compute Module 4"
	rPiCm4S    = rPi + " Compute Module 4 S"
	rPi5       = rPi + " 5"
)

// String returns the name of the Raspberry Pi model.
func (m Model) String() string {
	switch m {
	case NotPi:
		return ErrNotPi.Error()
	case Pi0:
		return rPiZero
	case Pi0W:
		return rPiZeroW
	case Pi02W:
		return rPiZero2W
	case Pi1A:
		return rPi1A
	case Pi1APlus:
		return rPi1APlus
	case Pi1B:
		return rPi1B
	case Pi1BPlus:
		return rPi1BPlus
	case Cm1:
		return rPiCm1
	case PiAlpha:
		return rPiAlpha
	case Pi2B:
		return rPi2B
	case Pi3B:
		return rPi3B
	case Pi3APlus:
		return rPi3APlus
	case Pi3BPlus:
		return rPi3BPlus
	case Cm3:
		return rPiCm3
	case Cm3Plus:
		return rPiCm3Plus
	case Pi4B:
		return rPi4B
	case Pi400:
		return rPi400
	case Cm4:
		return rPiCm4
	case Cm4S:
		return rPiCm4S
	case Pi5:
		return rPi5
	default:
		panic(fmt.Sprintf("invalid Raspberry Pi model: %d", int(m)))
	}
}

// ProbeError records problem probing the Raspberry Pi model.
type ProbeError struct {
	Err error
}

// Error implement the error interface.
func (e *ProbeError) Error() string {
	return fmt.Sprintf("cannot detect Raspberry Pi model: %s", e.Err)
}

// Unwrap returns the error wrapped inside the model probe error.
func (e *ProbeError) Unwrap() error {
	return e.Err
}

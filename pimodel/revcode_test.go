// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package pimodel_test

import (
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/pimodel"
)

func TestRevisionCode_String(t *testing.T) {
	// Both old and new style revisions are represented the way people might expect.
	testutil.Equal(t, pimodel.RevisionCode(0x0004).String(), "0004")
	testutil.Equal(t, pimodel.RevisionCode(0xa03140).String(), "a03140")
}

func TestRevisionCode_MarshalText(t *testing.T) {
	revCode := pimodel.RevisionCode(0x0004)
	data, err := revCode.MarshalText()
	testutil.Ok(t, err)
	testutil.SlicesEqual(t, data, []byte("0004"))

	revCode = pimodel.RevisionCode(0xa03140)
	data, err = revCode.MarshalText()
	testutil.Ok(t, err)
	testutil.SlicesEqual(t, data, []byte("a03140"))
}

func TestRevisionCode_UnmarshalText(t *testing.T) {
	var revCode pimodel.RevisionCode

	err := revCode.UnmarshalText([]byte("0004"))
	testutil.Ok(t, err)
	testutil.Equal(t, revCode, pimodel.RevisionCode(0x0004))

	err = revCode.UnmarshalText([]byte("a03111"))
	testutil.Ok(t, err)
	testutil.Equal(t, revCode, pimodel.RevisionCode(0xa03111))

	err = revCode.UnmarshalText([]byte("potato"))
	testutil.ErrorMatches(t, err, `strconv.ParseUint: parsing "potato": invalid syntax`)
}

func TestRevisionCode_KnownRevisionCodes(t *testing.T) {
	for _, tc := range []struct {
		code           pimodel.RevisionCode
		model          pimodel.Model
		boardType      uint8
		revision       string
		ram            uint
		manufacturer   string
		soc            pimodel.SoC
		warrantyVoid   bool
		notOtpRead     bool // negated value of CanReadOTP()
		notOtpWrite    bool // negated value of CanWriteOTP()
		notOvervoltage bool // negated value of CanUseOverVoltage()
	}{
		// Data from the "New-style revision codes in use" table on
		// https://www.raspberrypi.org/documentation/hardware/raspberrypi/revision-codes/README.md

		// Old style codes.

		{
			code:         0x0002,
			model:        pimodel.Pi1B,
			boardType:    0x01,
			revision:     "1.0",
			ram:          256,
			manufacturer: "Egoman",
			soc:          pimodel.BCM2835,
		},
		{
			code:         0x0003, /* same as 0x0002? */
			model:        pimodel.Pi1B,
			boardType:    0x01,
			revision:     "1.0",
			ram:          256,
			manufacturer: "Egoman",
		},
		{
			code:         0x0004,
			model:        pimodel.Pi1B,
			boardType:    0x01,
			revision:     "2.0",
			ram:          256,
			manufacturer: "Sony UK",
		},
		{
			code:         0x0005,
			model:        pimodel.Pi1B,
			boardType:    0x01,
			revision:     "2.0",
			ram:          256,
			manufacturer: "Qisda",
		},
		{
			code:         0x0006,
			model:        pimodel.Pi1B,
			boardType:    0x01,
			revision:     "2.0",
			ram:          256,
			manufacturer: "Egoman",
		},
		{
			code:         0x0007,
			model:        pimodel.Pi1A,
			boardType:    0x00,
			revision:     "2.0",
			ram:          256,
			manufacturer: "Egoman",
		},
		{
			code:         0x0008,
			model:        pimodel.Pi1A,
			boardType:    0x00,
			revision:     "2.0",
			ram:          256,
			manufacturer: "Sony UK",
		},
		{
			code:         0x0009,
			model:        pimodel.Pi1A,
			boardType:    0x00,
			revision:     "2.0",
			ram:          256,
			manufacturer: "Qisda",
		},
		{
			code:         0x000d,
			model:        pimodel.Pi1B,
			boardType:    0x01,
			revision:     "2.0",
			ram:          512,
			manufacturer: "Egoman",
		},
		{
			code:         0x000e,
			model:        pimodel.Pi1B,
			boardType:    0x01,
			revision:     "2.0",
			ram:          512,
			manufacturer: "Sony UK",
		},
		{
			code:         0x000f,
			model:        pimodel.Pi1B,
			boardType:    0x01,
			revision:     "2.0",
			ram:          512,
			manufacturer: "Egoman",
		},
		{
			code:         0x0010,
			model:        pimodel.Pi1BPlus,
			boardType:    0x03,
			revision:     "1.2",
			ram:          512,
			manufacturer: "Sony UK",
		},
		{
			code:         0x0011,
			model:        pimodel.Cm1,
			boardType:    0x06,
			revision:     "1.0",
			ram:          512,
			manufacturer: "Sony UK",
		},
		{
			code:         0x0012,
			model:        pimodel.Pi1APlus,
			boardType:    0x02,
			revision:     "1.1",
			ram:          256,
			manufacturer: "Sony UK",
		},
		{
			code:         0x0013,
			model:        pimodel.Pi1BPlus,
			boardType:    0x03,
			revision:     "1.2",
			ram:          512,
			manufacturer: "Embest",
		},
		{
			code:         0x0014,
			model:        pimodel.Cm1,
			boardType:    0x06,
			revision:     "1.0",
			ram:          512,
			manufacturer: "Embest",
		},
		{
			code:         0x0015,
			model:        pimodel.Pi1APlus,
			boardType:    0x02,
			revision:     "1.1",
			ram:          256,
			manufacturer: "Embest",
		},

		// New style codes.
		{
			code:         0x900021,
			model:        pimodel.Pi1APlus,
			boardType:    0x02,
			revision:     "1.1",
			ram:          512,
			manufacturer: "Sony UK",
		},
		{
			code:         0x900032,
			model:        pimodel.Pi1BPlus,
			boardType:    0x03,
			revision:     "1.2",
			ram:          512,
			manufacturer: "Sony UK",
		},
		{
			code:         0x900092,
			model:        pimodel.Pi0,
			boardType:    0x09,
			revision:     "1.2",
			ram:          512,
			manufacturer: "Sony UK",
		},
		{
			code:         0x900092,
			model:        pimodel.Pi0,
			boardType:    0x09,
			revision:     "1.2",
			ram:          512,
			manufacturer: "Sony UK",
		},
		{
			code:         0x900093,
			model:        pimodel.Pi0,
			boardType:    0x09,
			revision:     "1.3",
			ram:          512,
			manufacturer: "Sony UK",
		},
		{
			code:         0x9000c1,
			model:        pimodel.Pi0W,
			boardType:    0x0c,
			revision:     "1.1",
			ram:          512,
			manufacturer: "Sony UK",
		},
		{
			code:         0x9020e0,
			model:        pimodel.Pi3APlus,
			boardType:    0x0e,
			revision:     "1.0",
			ram:          512,
			manufacturer: "Sony UK",
		},
		{
			code:         0x920092,
			model:        pimodel.Pi0,
			boardType:    0x09,
			revision:     "1.2",
			ram:          512,
			manufacturer: "Embest",
		},
		{
			code:         0x920093,
			model:        pimodel.Pi0,
			boardType:    0x09,
			revision:     "1.3",
			ram:          512,
			manufacturer: "Embest",
		},
		{
			code:         0x900061,
			model:        pimodel.Cm1,
			boardType:    0x06,
			revision:     "1.1",
			ram:          512,
			manufacturer: "Sony UK",
		},
		{
			code:         0xa01040,
			model:        pimodel.Pi2B,
			boardType:    0x04,
			revision:     "1.0",
			ram:          1024,
			manufacturer: "Sony UK",
		},
		{
			code:         0xa01041,
			model:        pimodel.Pi2B,
			boardType:    0x04,
			revision:     "1.1",
			ram:          1024,
			manufacturer: "Sony UK",
		},
		{
			code:         0xa02082,
			model:        pimodel.Pi3B,
			boardType:    0x08,
			revision:     "1.2",
			ram:          1024,
			manufacturer: "Sony UK",
		},
		{
			code:         0xa020a0,
			model:        pimodel.Cm3,
			boardType:    0x0a,
			revision:     "1.0",
			ram:          1024,
			manufacturer: "Sony UK",
		},
		{
			code:         0xa020d3,
			model:        pimodel.Pi3BPlus,
			boardType:    0x0d,
			revision:     "1.3",
			ram:          1024,
			manufacturer: "Sony UK",
		},
		{
			code:         0xa02042,
			model:        pimodel.Pi2B,
			boardType:    0x04,
			revision:     "1.2",
			ram:          1024,
			manufacturer: "Sony UK",
			soc:          pimodel.BCM2837,
		},
		{
			code:         0xa21041,
			model:        pimodel.Pi2B,
			boardType:    0x04,
			revision:     "1.1",
			ram:          1024,
			manufacturer: "Embest",
		},
		{
			code:         0xa22042,
			model:        pimodel.Pi2B,
			boardType:    0x04,
			revision:     "1.2",
			ram:          1024,
			manufacturer: "Embest",
			soc:          pimodel.BCM2837,
		},
		{
			code:         0xa22082,
			model:        pimodel.Pi3B,
			boardType:    0x08,
			revision:     "1.2",
			ram:          1024,
			manufacturer: "Embest",
		},
		{
			code:         0xa220a0,
			model:        pimodel.Cm3,
			boardType:    0x0a,
			revision:     "1.0",
			ram:          1024,
			manufacturer: "Embest",
		},
		{
			code:         0xa32082,
			model:        pimodel.Pi3B,
			boardType:    0x08,
			revision:     "1.2",
			ram:          1024,
			manufacturer: "Sony Japan",
		},
		{
			code:         0xa52082,
			model:        pimodel.Pi3B,
			boardType:    0x08,
			revision:     "1.2",
			ram:          1024,
			manufacturer: "Stadium",
		},
		{
			code:         0xa22083,
			model:        pimodel.Pi3B,
			boardType:    0x08,
			revision:     "1.3",
			ram:          1024,
			manufacturer: "Embest",
		},
		{
			code:         0xa02100,
			model:        pimodel.Cm3Plus,
			boardType:    0x10,
			revision:     "1.0",
			ram:          1024,
			manufacturer: "Sony UK",

			// All the 3+ models have the B0 stepping.
			soc: pimodel.BCM2837B0,
		},
		{
			code:         0xa03111,
			model:        pimodel.Pi4B,
			boardType:    0x11,
			revision:     "1.1",
			ram:          1024,
			manufacturer: "Sony UK",
		},
		{
			code:         0xb03111,
			model:        pimodel.Pi4B,
			boardType:    0x11,
			revision:     "1.1",
			ram:          2048,
			manufacturer: "Sony UK",
		},
		{
			code:         0xb03112,
			model:        pimodel.Pi4B,
			boardType:    0x11,
			revision:     "1.2",
			ram:          2048,
			manufacturer: "Sony UK",
		},
		{
			code:         0xb03114,
			model:        pimodel.Pi4B,
			boardType:    0x11,
			revision:     "1.4",
			ram:          2048,
			manufacturer: "Sony UK",
		},
		{
			code:         0xc03111,
			model:        pimodel.Pi4B,
			boardType:    0x11,
			revision:     "1.1",
			ram:          4096,
			manufacturer: "Sony UK",
		},
		{
			code:         0xc03112,
			model:        pimodel.Pi4B,
			boardType:    0x11,
			revision:     "1.2",
			ram:          4096,
			manufacturer: "Sony UK",
		},
		{
			code:         0xc03114,
			model:        pimodel.Pi4B,
			boardType:    0x11,
			revision:     "1.4",
			ram:          4096,
			manufacturer: "Sony UK",
		},
		{
			code:         0xd03114,
			model:        pimodel.Pi4B,
			boardType:    0x11,
			revision:     "1.4",
			ram:          8192,
			manufacturer: "Sony UK",
		},
		{
			code:         0xc03115,
			model:        pimodel.Pi4B,
			boardType:    0x11,
			revision:     "1.5",
			ram:          4096,
			manufacturer: "Sony UK",
		},
		{
			code:         0xc03130,
			model:        pimodel.Pi400,
			boardType:    0x13,
			revision:     "1.0",
			ram:          4096,
			manufacturer: "Sony UK",
		},
		{
			code:         0xa03140,
			model:        pimodel.Cm4,
			boardType:    0x14,
			revision:     "1.0",
			ram:          1024,
			manufacturer: "Sony UK",
		},
		{
			code:         0xb03140,
			model:        pimodel.Cm4,
			boardType:    0x14,
			revision:     "1.0",
			ram:          2048,
			manufacturer: "Sony UK",
		},
		{
			code:         0xc03140,
			model:        pimodel.Cm4,
			boardType:    0x14,
			revision:     "1.0",
			ram:          4096,
			manufacturer: "Sony UK",
		},
		{
			code:         0xd03140,
			model:        pimodel.Cm4,
			boardType:    0x14,
			revision:     "1.0",
			ram:          8192,
			manufacturer: "Sony UK",
		},
		{
			code:         0x902120,
			model:        pimodel.Pi02W,
			boardType:    0x12,
			revision:     "1.0",
			ram:          512,
			manufacturer: "Sony UK",
			soc:          pimodel.RP3A0,
		},
		{
			code:         0xc04170,
			model:        pimodel.Pi5,
			boardType:    0x17,
			revision:     "1.0",
			ram:          4096,
			manufacturer: "Sony UK",
		},
		{
			code:         0xd04170,
			model:        pimodel.Pi5,
			boardType:    0x17,
			revision:     "1.0",
			ram:          8192,
			manufacturer: "Sony UK",
		},
	} {
		t.Run(tc.code.String(), func(t *testing.T) {
			model, err := tc.code.BoardType()
			testutil.Ok(t, err)
			testutil.Equal(t, model, tc.model)

			boardType, err := tc.code.BoardTypeCode()
			testutil.Ok(t, err)
			testutil.Equal(t, boardType, tc.boardType)

			revision, err := tc.code.BoardRevision()
			testutil.Ok(t, err)
			testutil.Equal(t, revision, tc.revision)

			ram, err := tc.code.MemorySize()
			testutil.Ok(t, err)
			testutil.Equal(t, ram, tc.ram)

			manufacturer, err := tc.code.Manufacturer()
			testutil.Ok(t, err)
			testutil.Equal(t, manufacturer, tc.manufacturer)

			soc, err := tc.code.Processor()
			testutil.Ok(t, err)
			// Check the SoC type if one is given in the test data.
			if tc.soc != pimodel.InvalidSoC {
				testutil.Equal(t, soc, tc.soc)
			}

			// Test the behavior only on new-style revision codes as the values are
			// not well-defined on onld-style code boards.
			if tc.code.IsNewStyle() {
				testutil.Equal(t, tc.code.WarrantyVoid(), tc.warrantyVoid)
				// Note the double negative, this is by design. It allows to avoid a lot of
				// non-zero values in the test data above.
				testutil.Equal(t, tc.code.CanUseOverVoltage(), !tc.notOvervoltage)
				testutil.Equal(t, tc.code.CanReadOTP(), !tc.notOtpRead)
				testutil.Equal(t, tc.code.CanWriteOTP(), !tc.notOtpWrite)
			}
		})
	}
}

func TestRevisionCode_WonkyMemorySize(t *testing.T) {
	_, err := pimodel.RevisionCode(0x0).MemorySize()
	testutil.ErrorMatches(t, err, `cannot decode revision code 0000, unknown memory size`)
}

func TestRevisionCode_WonkyManufacturer(t *testing.T) {
	_, err := pimodel.RevisionCode(0x0).Manufacturer()
	testutil.ErrorMatches(t, err, `cannot decode revision code 0000, unknown manufacturer`)
}

func TestRevisionCode_WonkyProcessor(t *testing.T) {
	_, err := pimodel.RevisionCode((5 << 12) | (1 << 23)).Processor() // 5th SoC | new flag
	testutil.ErrorMatches(t, err, `cannot decode revision code 805000, unknown processor`)
}

func TestRevisionCode_WonkyRevision(t *testing.T) {
	_, err := pimodel.RevisionCode(0x0001).BoardRevision()
	testutil.ErrorMatches(t, err, `cannot decode revision code 0001, unknown board revision`)
}

func TestRevisionCode_WonkyBoardType(t *testing.T) {
	_, err := pimodel.RevisionCode(0x0001).BoardType()
	testutil.ErrorMatches(t, err, `cannot decode revision code 0001, unknown board type`)
}

func TestProcCPUInfoFile(t *testing.T) {
	testutil.Equal(t, pimodel.ProcCPUInfoFile, "/proc/cpuinfo")
}

const piZeroCpuInfo = `processor	: 0
model name	: ARMv6-compatible processor rev 7 (v6l)
BogoMIPS	: 697.95
Features	: half thumb fastmult vfp edsp java tls
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xb76
CPU revision	: 7

Hardware	: BCM2835
Revision	: 9000c1
Serial		: 00000000a3896d54
Model		: Raspberry Pi Zero W Rev 1.1
`

const Pi1BCpuInfo = `processor	: 0
model name	: ARMv6-compatible processor rev 7 (v6l)
BogoMIPS	: 697.95
Features	: half thumb fastmult vfp edsp java tls
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xb76
CPU revision	: 7

Hardware	: BCM2835
Revision	: 000f
Serial		: 0000000054281887
Model		: Raspberry Pi Model B Rev 2
`

func TestProbeCpuInfo(t *testing.T) {
	d := t.TempDir()
	name := filepath.Join(d, "cpuinfo")

	_, _, err := pimodel.ProbeCPUInfo(name)
	testutil.ErrorMatches(t, err, `cannot detect Raspberry Pi model: open .*cpuinfo: .*`)
	testutil.ErrorIs(t, err, os.ErrNotExist)

	testutil.Ok(t, os.WriteFile(name, []byte(""), 0o600))
	_, _, err = pimodel.ProbeCPUInfo(name)
	testutil.ErrorMatches(t, err, `cannot detect Raspberry Pi model: not a Raspberry Pi`)

	testutil.Ok(t, os.WriteFile(name, []byte("Revision\t: potato"), 0o600))
	_, _, err = pimodel.ProbeCPUInfo(name)
	testutil.ErrorMatches(t, err, `cannot detect Raspberry Pi model: strconv.ParseUint: parsing "potato": invalid syntax`)

	testutil.Ok(t, os.WriteFile(name, []byte("Serial\t: tomato"), 0o600))
	_, _, err = pimodel.ProbeCPUInfo(name)
	testutil.ErrorMatches(t, err, `cannot detect Raspberry Pi model: strconv.ParseUint: parsing "tomato": invalid syntax`)

	testutil.Ok(t, os.WriteFile(name, []byte(piZeroCpuInfo), 0o600))
	code, serial, err := pimodel.ProbeCPUInfo(name)
	testutil.Ok(t, err)
	testutil.Equal(t, code, pimodel.RevisionCode(0x9000c1))
	testutil.Equal(t, serial, pimodel.SerialNumber(0x00000000a3896d54))

	testutil.Ok(t, os.WriteFile(name, []byte(Pi1BCpuInfo), 0o600))
	code, serial, err = pimodel.ProbeCPUInfo(name)
	testutil.Ok(t, err)
	testutil.Equal(t, code, pimodel.RevisionCode(0x000f))
	testutil.Equal(t, serial, pimodel.SerialNumber(0x0000000054281887))
}

func TestSerialNumber_String(t *testing.T) {
	num := pimodel.SerialNumber(0x00000000a3896d54)
	testutil.Equal(t, num.String(), "00000000a3896d54")
}

func TestSerialNumber_MarshalText(t *testing.T) {
	num := pimodel.SerialNumber(0x00000000a3896d54)
	data, err := num.MarshalText()
	testutil.Ok(t, err)
	testutil.SlicesEqual(t, data, []byte("00000000a3896d54"))
}

func TestSerialNumber_UnmarshalText(t *testing.T) {
	var num pimodel.SerialNumber

	err := num.UnmarshalText([]byte("00000000a3896d54"))
	testutil.Ok(t, err)
	testutil.Equal(t, num, pimodel.SerialNumber(0x00000000a3896d54))

	err = num.UnmarshalText([]byte("potato"))
	testutil.ErrorMatches(t, err, `strconv.ParseUint: parsing "potato": invalid syntax`)
}

// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package pimodel_test

import (
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/pimodel"
)

func TestModel_String(t *testing.T) {
	testutil.Equal(t, pimodel.NotPi.String(), "not a Raspberry Pi")

	testutil.Equal(t, pimodel.Pi0.String(), "Raspberry Pi Zero")
	testutil.Equal(t, pimodel.Pi0W.String(), "Raspberry Pi Zero W")
	testutil.Equal(t, pimodel.Pi02W.String(), "Raspberry Pi Zero 2 W")
	testutil.Equal(t, pimodel.Pi1A.String(), "Raspberry Pi Model A")
	testutil.Equal(t, pimodel.Pi1APlus.String(), "Raspberry Pi Model A Plus")
	testutil.Equal(t, pimodel.Cm1.String(), "Raspberry Pi Compute Module")
	testutil.Equal(t, pimodel.Pi1B.String(), "Raspberry Pi Model B")
	testutil.Equal(t, pimodel.Pi1BPlus.String(), "Raspberry Pi Model B Plus")
	testutil.Equal(t, pimodel.PiAlpha.String(), "Raspberry Pi Alpha")
	testutil.Equal(t, pimodel.Pi2B.String(), "Raspberry Pi 2 Model B")
	testutil.Equal(t, pimodel.Pi3B.String(), "Raspberry Pi 3 Model B")
	testutil.Equal(t, pimodel.Pi3APlus.String(), "Raspberry Pi 3 Model A Plus")
	testutil.Equal(t, pimodel.Pi3BPlus.String(), "Raspberry Pi 3 Model B Plus")
	testutil.Equal(t, pimodel.Cm3.String(), "Raspberry Pi Compute Module 3")
	testutil.Equal(t, pimodel.Cm3Plus.String(), "Raspberry Pi Compute Module 3 Plus")
	testutil.Equal(t, pimodel.Pi4B.String(), "Raspberry Pi 4 Model B")
	testutil.Equal(t, pimodel.Pi400.String(), "Raspberry Pi 400")
	testutil.Equal(t, pimodel.Cm4.String(), "Raspberry Pi Compute Module 4")
	testutil.Equal(t, pimodel.Cm4S.String(), "Raspberry Pi Compute Module 4 S")
	testutil.Equal(t, pimodel.Pi5.String(), "Raspberry Pi 5")

	testutil.PanicMatches(t, func() { _ = pimodel.Model(42).String() }, "invalid Raspberry Pi model: 42")
}

// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package pimodel

import "fmt"

// SoC represents the model of the Broadcom system-on-chip.
//
// Available processors are documented at
// https://www.raspberrypi.org/documentation/computers/processors.html
type SoC int

const (
	// InvalidSoC represents a non-Raspberry Pi processor.
	InvalidSoC SoC = iota
	// BCM2835 is used in the Raspberry Pi Model A, B, A+, B+ and the Raspberry Pi Zero.
	BCM2835
	// BCM2836 is used in the Raspberry Pi Model 2B.
	BCM2836
	// BCM2837 is used in all models of the Raspberry Pi 3 and later models of the Raspberry Pi 2.
	// It is also known as BCM2837A0, to differentiate it from the BCM2837B0 sibling.
	BCM2837
	// BCM2837B0 is used in the Raspberry Pi 3 Model B+ and A+.
	BCM2837B0
	// BCM2711 is used in the Raspberry Pi 4 Model B.
	BCM2711
	// RP3A0 is used in the Raspberry Pi Zero 2 W.
	RP3A0
	// BCM2712 is used in the Raspberry Pi 5.
	BCM2712
)

// String returns the name of the System-on-Chip.
func (soc SoC) String() string {
	switch soc {
	case InvalidSoC:
		return "invalid"
	case BCM2835:
		return "BCM2835"
	case BCM2836:
		return "BCM2836"
	case BCM2837:
		return "BCM2837"
	case BCM2837B0:
		return "BCM2837B0"
	case BCM2711:
		return "BCM2711"
	case BCM2712:
		return "BCM2712"
	case RP3A0:
		return "RP3A0"
	default:
		panic(fmt.Sprintf("cannot find name of SoC %#x", uint(soc)))
	}
}

// BcmSiliconFamilyCode returns the SoC family code.
//
// A given package, like 2835, belongs to a given family, like 2708.
// This distinction is relevant to locate the right device tree file.
func (soc SoC) BcmSiliconFamilyCode() int {
	switch soc {
	case BCM2835:
		return 2708
	case BCM2836:
		return 2709
	case BCM2837, BCM2837B0, RP3A0:
		return 2710
	case BCM2711:
		return 2711
	case BCM2712:
		return 2712
	default:
		panic(fmt.Sprintf("cannot find silicon family code of SoC %#x", uint(soc)))
	}
}

// BcmPackageCode returns the SoC package code.
//
// See BcmSiliconFamilyCode for comparison.
func (soc SoC) BcmPackageCode() int {
	switch soc {
	case BCM2835:
		return 2835
	case BCM2836:
		return 2836
	case BCM2837, BCM2837B0, RP3A0:
		return 2837
	case BCM2711:
		return 2711
	case BCM2712:
		return 2712
	default:
		panic(fmt.Sprintf("cannot find package code of SoC %#x", uint(soc)))
	}
}

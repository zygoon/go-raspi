// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package pimodel

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// RevisionCode encodes properties of a Raspberry Pi board.
//
// There are two types of revision codes, old style, with a side lookup table
// for each property. New style use a longer revision code to encode particular
// properties as bit-fields.
//
// Revision codes are useful to know the exact SoC used, which determines file
// name of the compiled device tree file that is necessary to boot.
//
// Revision codes are documented in detail at.
// https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#raspberry-pi-revision-codes
type RevisionCode uint

const (
	// Board revision field, 4 least significant bits.
	revisionWidth = 4
	revisionShift = 0
	revisionMask  = (1 << revisionWidth) - 1

	// Board type field, 8 subsequent bits.
	typeWidth = 8
	typeShift = revisionShift + revisionWidth
	typeMask  = (1 << typeWidth) - 1

	// Processor type field, 4 subsequent bits.
	processorWidth = 4
	processorShift = typeShift + typeWidth
	processorMask  = (1 << processorWidth) - 1

	// Board manufacturer field, 4 subsequent bits.
	manufacturerWidth = 4
	manufacturerShift = processorShift + processorWidth
	manufacturerMask  = (1 << manufacturerWidth) - 1

	// Memory size field, 3 subsequent bits.
	memorySizeWidth = 3
	memorySizeShift = manufacturerShift + manufacturerWidth
	memorySizeMask  = (1 << memorySizeWidth) - 1

	// New-style-revision flag, one subsequent bit.
	newFlagWidth = 1
	newFlagMask  = (1 << newFlagWidth) - 1
	newFlagShift = memorySizeShift + memorySizeWidth

	unused1Width = 1
	unused1Shift = newFlagShift + newFlagWidth

	warrantyWidth = 1
	warrantyMask  = (1<<warrantyWidth - 1)
	warrantyShift = unused1Shift + unused1Width

	unused2Width = 3
	unused2Shift = warrantyShift + warrantyWidth

	otpReadWidth = 1
	otpReadMask  = (1<<otpReadWidth - 1)
	otpReadShift = unused2Shift + unused2Width

	otpWriteWidth = 1
	otpWriteMask  = (1<<otpWriteWidth - 1)
	otpWriteShift = otpReadShift + otpReadWidth

	overVoltageWidth = 1
	overVoltageMask  = (1<<overVoltageWidth - 1)
	overVoltageShift = otpWriteShift + otpWriteWidth
)

// String returns the revision code as a hexadecimal string.
func (code RevisionCode) String() string {
	// The selected format is that of /proc/cpuinfo.
	if code < 16 {
		return fmt.Sprintf("%04x", uint(code))
	}

	return fmt.Sprintf("%06x", uint(code))
}

// MarshalText implements encoding.TextMarshaler.
func (code RevisionCode) MarshalText() ([]byte, error) {
	return []byte(code.String()), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (code *RevisionCode) UnmarshalText(text []byte) error {
	n, err := strconv.ParseUint(string(text), 16, 32)
	if err != nil {
		return err
	}

	*code = RevisionCode(n)

	return nil
}

// IsNewStyle returns true if the revision code uses new-style format.
func (code RevisionCode) IsNewStyle() bool {
	return (code>>newFlagShift)&newFlagMask == 1
}

// MemorySize returns the amount of memory, in megabytes, encoded in the revision code.
func (code RevisionCode) MemorySize() (uint, error) {
	// Consult https://www.raspberrypi.org/documentation/hardware/raspberrypi/revision-codes/README.md
	// for details on how to decode the revision code.
	if code.IsNewStyle() {
		switch (code >> memorySizeShift) & memorySizeMask {
		case 0:
			return 256, nil
		case 1:
			return 512, nil
		case 2:
			return 1024, nil
		case 3:
			return 2048, nil
		case 4:
			return 4096, nil
		case 5:
			return 8192, nil
		}
	} else {
		// Old style revision code.
		switch code {
		case 0x0002, 0x0003, 0x0004, 0x0005, 0x0006, 0x0007, 0x0008, 0x0009, 0x0012:
			return 256, nil
		case 0x000d, 0x000e, 0x000f, 0x0010, 0x0011, 0x0013, 0x0014:
			return 512, nil
		case 0x0015:
			// Uncertain amount, could be 512, pick the conservative amount.
			return 256, nil
		}
	}

	return 0, &RevisionDecodeError{RevisionCode: code, Field: fieldMemorySize}
}

const (
	mfcSonyUK  = "Sony UK"
	mfcEgoman  = "Egoman"
	mfcSonyJP  = "Sony Japan"
	mfcEmbest  = "Embest"
	mfcStadium = "Stadium"
	mfcQisda   = "Qisda"
)

// Manufacturer returns the name of the manufacturer encoded in the revision code.
func (code RevisionCode) Manufacturer() (string, error) {
	if code.IsNewStyle() {
		switch (code >> manufacturerShift) & manufacturerMask {
		case 0:
			return mfcSonyUK, nil
		case 1:
			return mfcEgoman, nil
		case 2, 4:
			return mfcEmbest, nil
		case 3:
			return mfcSonyJP, nil
		case 5:
			return mfcStadium, nil
		}
	} else {
		// Old style revision code.
		switch code {
		case 0x0002, 0x0003, 0x0006, 0x0007, 0x000d, 0x000f:
			return mfcEgoman, nil
		case 0x0004, 0x0008, 0x000e, 0x0010, 0x0011, 0x0012:
			return mfcSonyUK, nil
		case 0x0005, 0x0009:
			return mfcQisda, nil
		case 0x0013, 0x0014, 0x0015:
			return mfcEmbest, nil
		}
	}

	return "", &RevisionDecodeError{RevisionCode: code, Field: fieldManufacturer}
}

// Processor returns the type of System-on-Chip encoded in the revision code.
func (code RevisionCode) Processor() (SoC, error) {
	if code.IsNewStyle() {
		switch (code >> processorShift) & processorMask {
		case 0:
			return BCM2835, nil
		case 1:
			return BCM2836, nil
		case 2:
			if model, err := code.BoardType(); err == nil {
				switch model {
				case Pi3APlus, Pi3BPlus, Cm3Plus:
					return BCM2837B0, nil
				case Pi02W:
					return RP3A0, nil
				}
			}

			return BCM2837, nil
		case 3:
			return BCM2711, nil
		case 4:
			return BCM2712, nil
		}
	} else {
		// All the boards using old-style revision code use the same SoC.
		return BCM2835, nil
	}

	return InvalidSoC, &RevisionDecodeError{RevisionCode: code, Field: fieldProcessor}
}

// BoardType returns the model of the Raspberry Pi board encoded in the revision code.
func (code RevisionCode) BoardType() (Model, error) {
	boardType, err := code.BoardTypeCode()
	if err != nil {
		return NotPi, err
	}

	switch boardType {
	case 0x00:
		return Pi1A, nil
	case 0x01:
		return Pi1B, nil
	case 0x02:
		return Pi1APlus, nil
	case 0x03:
		return Pi1BPlus, nil
	case 0x04:
		return Pi2B, nil
	case 0x05:
		return PiAlpha, nil
	case 0x06:
		return Cm1, nil
	// 0x07 is not documented
	case 0x08:
		return Pi3B, nil
	case 0x09:
		return Pi0, nil
	case 0x0a:
		return Cm3, nil
	// 0x0b is not documented
	case 0x0c:
		return Pi0W, nil
	case 0x0d:
		return Pi3BPlus, nil
	case 0x0e:
		return Pi3APlus, nil
	// 0x0f is documented as internal use only
	case 0x10:
		return Cm3Plus, nil
	case 0x11:
		return Pi4B, nil
	case 0x12:
		return Pi02W, nil
	case 0x13:
		return Pi400, nil
	case 0x14:
		return Cm4, nil
	case 0x15:
		return Cm4S, nil
	// 0x16 is documented as internal use only
	case 0x17:
		return Pi5, nil
	}

	return NotPi, &RevisionDecodeError{RevisionCode: code, Field: fieldBoardType}
}

// BoardTypeCode returns the board type part of the revision code.
//
// New-style revision codes encode the board type directly. Old-style revision
// codes do not, returned values are synthesized based on a lookup table.
func (code RevisionCode) BoardTypeCode() (uint8, error) {
	if code.IsNewStyle() {
		return uint8((code >> typeShift) & typeMask), nil
	}

	// Old style revision code.
	switch code {
	case 0x0007, 0x0008, 0x0009:
		return 0x00, nil // board type code for Pi 1A
	case 0x0002, 0x0003, 0x0004, 0x0005, 0x0006, 0x000d, 0x000e, 0x000f:
		return 0x01, nil // board type code for Pi 1B
	case 0x0012, 0x0015:
		return 0x02, nil // board type code for Pi 1A+
	case 0x0010, 0x0013:
		return 0x03, nil // board type code for Pi 1B+
	case 0x0011, 0x0014:
		return 0x06, nil // board type code for CM1
	}

	return 0, &RevisionDecodeError{RevisionCode: code, Field: fieldBoardType}
}

// BoardRevision returns the board revision string.
func (code RevisionCode) BoardRevision() (string, error) {
	if code.IsNewStyle() {
		return fmt.Sprintf("1.%d", uint(code>>revisionShift)&revisionMask), nil
	}

	switch code {
	// Old style revision code.
	case 0x0002, 0x0003, 0x0011, 0x0014:
		return "1.0", nil
	case 0x0004, 0x0005, 0x0006, 0x0007, 0x0008, 0x0009, 0x000d, 0x000e, 0x000f:
		return "2.0", nil
	case 0x0010, 0x0013:
		return "1.2", nil
	case 0x0012, 0x0015:
		return "1.1", nil
	}

	return "", &RevisionDecodeError{RevisionCode: code, Field: fieldBoardRevision}
}

// WarrantyVoid returns true if the warranty has been void by over-voltage.
func (code RevisionCode) WarrantyVoid() bool {
	if !code.IsNewStyle() {
		// XXX: Conservative, assuming this cannot be done on old-style boards.
		return false
	}

	return (code>>warrantyShift)&warrantyMask == 1
}

// CanUseOverVoltage retruns true if over-voltage is allowed.
func (code RevisionCode) CanUseOverVoltage() bool {
	if !code.IsNewStyle() {
		// XXX: Conservative, assuming this cannot be done on old-style boards.
		return false
	}

	switch (code >> overVoltageShift) & overVoltageMask {
	case 0: // Over-voltage is allowed.
		return true
	default: // Over-voltage is not allowed.
		return false
	}
}

// CanReadOPT returns true if reading One-Time-Programmable (OTP) bits is allowed.
func (code RevisionCode) CanReadOTP() bool {
	if !code.IsNewStyle() {
		// XXX: Conservative, assuming this cannot be done on old-style boards.
		return false
	}

	switch (code >> otpReadShift) & otpReadMask {
	case 0: // Reading OTP bits is allowed.
		return true
	default: // Reading OTP bits is not allowed.
		return false
	}
}

// CanWriteOPT returns true if writing One-Time-Programmable (OTP) bits is allowed.
func (code RevisionCode) CanWriteOTP() bool {
	if !code.IsNewStyle() {
		// XXX: Conservative, assuming this cannot be done on old-style boards.
		return false
	}

	switch (code >> otpWriteShift) & otpWriteMask {
	case 0: // Writing OTP bits is allowed.
		return true
	default: // Writing OTP bits is not allowed.
		return false
	}
}

const (
	fieldBoardRevision = "board revision"
	fieldBoardType     = "board type"
	fieldManufacturer  = "manufacturer"
	fieldMemorySize    = "memory size"
	fieldProcessor     = "processor"
)

// RevisionDecodeError records inability to decode a given revision code.
type RevisionDecodeError struct {
	RevisionCode RevisionCode
	// Field is one of "board revision", "board type", "manufacturer", "memory size" or "processor".
	Field string
}

// Error implements the error interface.
func (e *RevisionDecodeError) Error() string {
	return fmt.Sprintf("cannot decode revision code %v, unknown %s", e.RevisionCode, e.Field)
}

// ErrNotPi records probe failure when invoked on a device other than a Raspberry Pi.
var ErrNotPi = errors.New("not a Raspberry Pi")

// ProcCPUInfoFile is the name of procfs file containing CPU information.
const ProcCPUInfoFile = "/proc/cpuinfo"

// SerialNumber is the serial number of the SoC.
type SerialNumber uint64

// String implements the stringer interface.
func (serialNum SerialNumber) String() string {
	// The selected format is that of /proc/cpuinfo.
	return fmt.Sprintf("%016x", uint64(serialNum))
}

// MarshalText implements encoding.TextMarshaler.
func (serialNum SerialNumber) MarshalText() ([]byte, error) {
	return []byte(serialNum.String()), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (serialNum *SerialNumber) UnmarshalText(text []byte) error {
	n, err := strconv.ParseUint(string(text), 16, 64)
	if err != nil {
		return err
	}

	*serialNum = SerialNumber(n)

	return nil
}

// ProbeCPUInfo returns the serial number and revision code from /proc/cpuinfo..
//
// On running Raspberry Pi systems please call this function with
// ProcCPUInfoFile argument.
func ProbeCPUInfo(fileName string) (RevisionCode, SerialNumber, error) {
	f, err := os.Open(fileName)
	if err != nil {
		return 0, 0, &ProbeError{Err: err}
	}

	defer func() {
		_ = f.Close()
	}()

	var revCode RevisionCode

	var serialNum SerialNumber

	s := bufio.NewScanner(f)
	for s.Scan() {
		line := s.Text()
		if idx := strings.IndexRune(line, ':'); idx != -1 {
			switch {
			case strings.HasPrefix(line, "Revision\t"):
				val, err := strconv.ParseUint(strings.TrimSpace(line[idx+1:]), 16, 32)
				if err != nil {
					return 0, 0, &ProbeError{Err: err}
				}

				revCode = RevisionCode(val)
			case strings.HasPrefix(line, "Serial\t"):
				val, err := strconv.ParseUint(strings.TrimSpace(line[idx+1:]), 16, 64)
				if err != nil {
					return 0, 0, &ProbeError{Err: err}
				}

				serialNum = SerialNumber(val)
			}
		}
	}

	if err := s.Err(); err != nil {
		return 0, 0, &ProbeError{Err: err}
	}

	if revCode == RevisionCode(0) {
		return 0, 0, &ProbeError{Err: ErrNotPi}
	}

	return revCode, serialNum, nil
}

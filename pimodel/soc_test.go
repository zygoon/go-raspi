// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package pimodel_test

import (
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/pimodel"
)

func TestSoC_String(t *testing.T) {
	testutil.Equal(t, pimodel.InvalidSoC.String(), "invalid")

	testutil.Equal(t, pimodel.BCM2835.String(), "BCM2835")
	testutil.Equal(t, pimodel.BCM2836.String(), "BCM2836")
	testutil.Equal(t, pimodel.BCM2837.String(), "BCM2837")
	testutil.Equal(t, pimodel.BCM2837B0.String(), "BCM2837B0")
	testutil.Equal(t, pimodel.BCM2711.String(), "BCM2711")
	testutil.Equal(t, pimodel.BCM2712.String(), "BCM2712")
	testutil.Equal(t, pimodel.RP3A0.String(), "RP3A0")

	testutil.PanicMatches(t, func() { _ = pimodel.SoC(42).String() }, "cannot find name of SoC 0x2a")
}

func TestSoC_BcmSiliconFamilyCode(t *testing.T) {
	testutil.Equal(t, pimodel.BCM2835.BcmSiliconFamilyCode(), 2708)
	testutil.Equal(t, pimodel.BCM2836.BcmSiliconFamilyCode(), 2709)
	testutil.Equal(t, pimodel.BCM2837.BcmSiliconFamilyCode(), 2710)
	testutil.Equal(t, pimodel.BCM2837B0.BcmSiliconFamilyCode(), 2710)
	testutil.Equal(t, pimodel.BCM2711.BcmSiliconFamilyCode(), 2711)
	testutil.Equal(t, pimodel.BCM2712.BcmSiliconFamilyCode(), 2712)
	testutil.Equal(t, pimodel.RP3A0.BcmSiliconFamilyCode(), 2710)

	testutil.PanicMatches(t, func() { _ = pimodel.SoC(42).BcmSiliconFamilyCode() }, "cannot find silicon family code of SoC 0x2a")
}

func TestSoC_BcmPackageCode(t *testing.T) {
	testutil.Equal(t, pimodel.BCM2835.BcmPackageCode(), 2835)
	testutil.Equal(t, pimodel.BCM2836.BcmPackageCode(), 2836)
	testutil.Equal(t, pimodel.BCM2837.BcmPackageCode(), 2837)
	testutil.Equal(t, pimodel.BCM2837B0.BcmPackageCode(), 2837)
	testutil.Equal(t, pimodel.BCM2711.BcmPackageCode(), 2711)
	testutil.Equal(t, pimodel.BCM2712.BcmPackageCode(), 2712)
	testutil.Equal(t, pimodel.RP3A0.BcmPackageCode(), 2837)

	testutil.PanicMatches(t, func() { _ = pimodel.SoC(42).BcmPackageCode() }, "cannot find package code of SoC 0x2a")
}

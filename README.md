<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Huawei Inc.
-->

# About the Go Raspberry Pi module

This module has been split out of the SystemOTA or SysOTA project, where it
provided essential services for native support of the Raspberry Pi bootloader.

The package is clearly oriented towards Raspberry Pi boot facilities, but it
may grow additional feature modules over time.

## Overview of packages

The `piboot` package provides very a way to look at kernel command line,
and parse kernel arguments in a way useful for the boot system.

The `pimodel` package provides several types for identifying the particular
Raspberry Pi board and SoC as well as identifiers for each known Raspberry Pi
device.

The `picfg` package provides support for reading `config.txt`-style boot
configuration files. The package allows loading, interpreting and modifying the
configuration at will and is aimed at programmatic boot system analysis and
reconfiguration.

The boot configuration system supports the so-called conditional filters, where
certain configuration parameters applied conditionally. Those are fully
supported by the `picfg/condfilter` package. The package provides types for
each conditional filter, matching functions and a firmware behavior simulator,
which can predict how the firmware would interpret the conditional sections.
The simulator can be also used to query boot configuration either for the
simulated board or in a _coherent_ mode, where a given parameter has the same
value regardless of the board type.

Lastly the `pieeprom` package provides initial support for querying the boot
EEPROM present on some boards. This package will likely grow to support
retrieving implicit EEPROM include, so that boot configuration can be fully
understood.

## Known limitations

There are several things that are known and not implemented yet. Those are:

- The `picfg` package does not support the firmware include syntax yet.
- The `picfg` and `pieeprom` packages do not support the implicit include of
  the EEPROM configuration text yet.
- There is no package which allows guessing which boot files are required given
  the board revision code and boot configuration. This is useful to verify that
  the system is in a state consistent for booting but is fairly complex to
  implement as it relies on some black-box testing of the firmware behavior at
  runtime.

## Contributions

Contributions are welcome. Please try to respect Go requirements (1.22 at the
moment) and the overall coding and testing style.

## License and REUSE

This project is licensed under the Apache 2.0 license, see the `LICENSE` file
for details. The project is compliant with https://reuse.software/ - making it
easy to ensure license and copyright compliance by automating software
bill-of-materials. In other words, it's a good citizen in the modern free
software stacks.

// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package piboot contains code specific to the Raspberry Pi boot system.
package piboot

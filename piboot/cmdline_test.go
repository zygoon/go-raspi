// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package piboot_test

import (
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	boot "gitlab.com/zygoon/go-raspi/piboot"
)

func TestCmdLine_Flags(t *testing.T) {
	var cl boot.CmdLine

	cl.SetFlag("foo")
	testutil.Equal(t, string(cl), "foo")

	cl.SetFlag("bar")
	testutil.Equal(t, string(cl), "foo bar")

	cl.SetFlag("foo")
	testutil.Equal(t, string(cl), "foo bar")

	testutil.Equal(t, cl.HasFlag("foo"), true)
	testutil.Equal(t, cl.HasFlag("bar"), true)
	testutil.Equal(t, cl.HasFlag("froz"), false)

	cl.DelFlag("foo")
	testutil.Equal(t, string(cl), "bar")

	cl.DelFlag("froz")
	testutil.Equal(t, string(cl), "bar")

	cl.DelFlag("bar")
	testutil.Equal(t, string(cl), "")
}

func TestCmdLine_Args(t *testing.T) {
	var cl boot.CmdLine

	cl.SetArg("foo", "value")
	testutil.Equal(t, string(cl), "foo=value")

	cl.SetArg("bar", "")
	testutil.Equal(t, string(cl), "foo=value bar=")

	cl.SetArg("foo", "new-value")
	testutil.Equal(t, string(cl), "foo=new-value bar=")

	testutil.SlicesEqual(t, cl.ArgValues("foo"), []string{"new-value"})
	testutil.SlicesEqual(t, cl.ArgValues("bar"), []string{""})
	testutil.SlicesEqual(t, cl.ArgValues("froz"), []string(nil))

	cl.DelArg("foo")
	testutil.Equal(t, string(cl), "bar=")

	cl.DelArg("froz")
	testutil.Equal(t, string(cl), "bar=")

	cl.DelArg("bar")
	testutil.Equal(t, string(cl), "")
}

func TestCmdLine_ArgValue(t *testing.T) {
	cl := boot.CmdLine("foo=value bar= quux=value-1 quux=value-2")

	val, err := cl.ArgValue("foo")
	testutil.Ok(t, err)
	testutil.Equal(t, val, "value")

	val, err = cl.ArgValue("bar")
	testutil.Ok(t, err)
	testutil.Equal(t, val, "")

	_, err = cl.ArgValue("froz")
	testutil.ErrorMatches(t, err, `argument not found`)

	_, err = cl.ArgValue("quux")
	testutil.ErrorMatches(t, err, `argument has many values`)
}

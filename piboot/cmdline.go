// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package piboot

import (
	"errors"
	"strings"
)

// CmdLine represents kernel command line.
type CmdLine string

// HasFlag returns true if the command line has a given flag.
func (cl CmdLine) HasFlag(flagName string) bool {
	fields := strings.Fields(string(cl))

	for _, field := range fields {
		if field == flagName {
			return true
		}
	}

	return false
}

// SetFlag sets a flag argument in the kernel command line.
//
// SetFlag scans through the arguments, looking for the given flag. If an
// existing flag is found SetFlag does nothing, otherwise the desired flag is
// appended.
func (cl *CmdLine) SetFlag(flagName string) {
	fields := strings.Fields(string(*cl))

	for _, field := range fields {
		if field == flagName {
			return
		}
	}

	fields = append(fields, flagName)
	*cl = CmdLine(strings.Join(fields, " "))
}

// DelFlag deletes the given flag from command line.
func (cl *CmdLine) DelFlag(flagName string) {
	fields := strings.Fields(string(*cl))
	fieldsOut := make([]string, 0, len(fields))

	for _, field := range fields {
		if field != flagName {
			fieldsOut = append(fieldsOut, field)
		}
	}

	*cl = CmdLine(strings.Join(fieldsOut, " "))
}

// ArgValues returns all the values of a named argument in kernel command line.
//
// If the given named argument is not found, the return is a nil slice. If there
// are multiple arguments with the same name, the return value is a slice of all
// their values.
//
// argName should end with the '=' rune.
func (cl CmdLine) ArgValues(argName string) (values []string) {
	if !strings.HasSuffix(argName, "=") {
		argName = argName + "="
	}

	for _, field := range strings.Fields(string(cl)) {
		if strings.HasPrefix(field, argName) {
			values = append(values, strings.TrimPrefix(field, argName))
		}
	}

	return values
}

// SetArg sets a named argument in the kernel command line.
//
// SetArg scans through the arguments, looking for a prefix match. Each matching
// occurrence is replaced in place. If matches were found SetArg appends the
// desired argument and value.
//
// argName should end with the '=' rune.
func (cl *CmdLine) SetArg(argName, argValue string) {
	if !strings.HasSuffix(argName, "=") {
		argName = argName + "="
	}

	replaced := false
	fields := strings.Fields(string(*cl))

	for idx, field := range fields {
		if strings.HasPrefix(field, argName) {
			fields[idx] = argName + argValue
			replaced = true
		}
	}

	if !replaced {
		fields = append(fields, argName+argValue)
	}

	*cl = CmdLine(strings.Join(fields, " "))
}

// DelArg deletes a named argument in the kernel command line.
//
// argName should end with the '=' rune.
func (cl *CmdLine) DelArg(argName string) {
	if !strings.HasSuffix(argName, "=") {
		argName = argName + "="
	}

	fields := strings.Fields(string(*cl))
	fieldsOut := make([]string, 0, len(fields))

	for _, field := range fields {
		if !strings.HasPrefix(field, argName) {
			fieldsOut = append(fieldsOut, field)
		}
	}

	*cl = CmdLine(strings.Join(fieldsOut, " "))
}

var (
	// ErrMissingArgument reports case where a named argument is missing.
	ErrMissingArgument = errors.New("argument not found")
	// ErrRepeatedArgument reports case when a named argument is repeated.
	ErrRepeatedArgument = errors.New("argument has many values")
)

// ArgValue returns the value of a named argument in kernel command line.
//
// If the argument is not found ErrMissingArgument is returned.
// The argument is used multiple times, ErrRepeatedArgument is returned.
func (cl CmdLine) ArgValue(argName string) (string, error) {
	values := cl.ArgValues(argName)
	switch len(values) {
	case 0:
		return "", ErrMissingArgument
	case 1:
		return values[0], nil
	default:
		return "", ErrRepeatedArgument
	}
}

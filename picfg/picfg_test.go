// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package picfg_test

import (
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/picfg"
)

// vanillaUbuntuHirsuteBootConfigTxt is the text of /boot/firmware/config.txt on Ubuntu 21.04.
const vanillaUbuntuHirsuteBootConfigTxt = `
[pi4]
max_framebuffers=2

[all]
kernel=vmlinuz
cmdline=cmdline.txt
initramfs initrd.img followkernel

# Enable the audio output, I2C and SPI interfaces on the GPIO header
dtparam=audio=on
dtparam=i2c_arm=on
dtparam=spi=on

# Enable the KMS ("full" KMS) graphics overlay, and allocate 128Mb to the GPU
# memory. The full KMS overlay is required for X11 application support under
# wayland
dtoverlay=vc4-kms-v3d
gpu_mem=128

# Uncomment the following to enable the Raspberry Pi camera module firmware.
# Be warned that there *may* be incompatibilities with the "full" KMS overlay
#start_x=1

# Comment out the following line if the edges of the desktop appear outside
# the edges of your display
disable_overscan=1

# If you have issues with audio, you may try uncommenting the following line
# which forces the HDMI output into HDMI mode instead of DVI (which doesn't
# support audio output)
#hdmi_drive=2

# If you have a CM4, uncomment the following line to enable the USB2 outputs
# on the IO board (assuming your CM4 is plugged into such a board)
#dtoverlay=dwc2,dr_mode=host

# Config settings specific to arm64
arm_64bit=1
dtoverlay=dwc2
`

// stripLocation strips location from a ConfigTxt and all the sections and parameters it contains.
func stripLocation(configTxt picfg.ConfigTxt) {
	for sectIdx := range configTxt.Sections {
		configTxt.Sections[sectIdx].Location = picfg.Location{}
		for paramIdx := range configTxt.Sections[sectIdx].Parameters {
			configTxt.Sections[sectIdx].Parameters[paramIdx].Location = picfg.Location{}
		}
	}
}

func TestConfigTxt_UnmarshalMarshalUnmarshalRoundTrip(t *testing.T) {
	var incoming, outgoing picfg.ConfigTxt

	incomingText := []byte(vanillaUbuntuHirsuteBootConfigTxt)
	err := incoming.UnmarshalText(incomingText)
	testutil.Ok(t, err)

	// Strip, location, as it is not preserved in round trip due to comments being removed.
	stripLocation(incoming)

	intermediateText, err := incoming.MarshalText()
	testutil.Ok(t, err)

	err = outgoing.UnmarshalText(intermediateText)
	testutil.Ok(t, err)

	stripLocation(outgoing)

	testutil.ObjectEqual(t, &incoming, &outgoing)
	outgoingText, err := outgoing.MarshalText()
	testutil.Ok(t, err)
	testutil.Equal(t, string(outgoingText), ""+
		"[pi4]\n"+
		"max_framebuffers=2\n"+
		"[all]\n"+
		"kernel=vmlinuz\n"+
		"cmdline=cmdline.txt\n"+
		"initramfs initrd.img followkernel\n"+
		"dtparam=audio=on\n"+
		"dtparam=i2c_arm=on\n"+
		"dtparam=spi=on\n"+
		"dtoverlay=vc4-kms-v3d\n"+
		"gpu_mem=128\n"+
		"disable_overscan=1\n"+
		"arm_64bit=1\n"+
		"dtoverlay=dwc2\n")
}

func TestParameter_MarshalText(t *testing.T) {
	p := picfg.Parameter{Name: "key", Value: "value"}
	data, err := p.MarshalText()
	testutil.Ok(t, err)
	testutil.Equal(t, string(data), "key=value")

	p = picfg.Parameter{Name: "initramfs", Value: "value"}
	data, err = p.MarshalText()
	testutil.Ok(t, err)
	testutil.Equal(t, string(data), "initramfs value")
}

func TestParameter_UnmarshalText(t *testing.T) {
	var p picfg.Parameter
	err := p.UnmarshalText([]byte(`key=value`))
	testutil.Ok(t, err)
	testutil.ObjectEqual(t, &p, &picfg.Parameter{Name: "key", Value: "value"})

	// initramfs uses special syntax which deviates from key=value
	err = p.UnmarshalText([]byte(`initramfs initrd.img followkernel`))
	testutil.Ok(t, err)
	testutil.ObjectEqual(t, &p, &picfg.Parameter{
		Name:  "initramfs",
		Value: "initrd.img followkernel",
	})
}

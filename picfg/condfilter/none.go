// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter

// NoneFilter is a config.txt conditional filter ignored by the boot firmware.
//
// Any configuration options placed in the [none] section are ignored by the firmware.
type NoneFilter struct{}

// None is an instance of NoneFilter.
var None = NoneFilter{}

// MarshalText implements TextMarshaler.
func (none NoneFilter) MarshalText() ([]byte, error) {
	return []byte(none.ConditionalFilter()), nil
}

// UnmarshalText implements TextUnmarshaler.
func (none *NoneFilter) UnmarshalText(text []byte) error {
	const typ = "NoneFilter"

	name, err := parseName(text, typ)
	if err != nil {
		return err
	}

	if name != "none" {
		return &FilterError{Type: typ, Filter: string(text), Err: ErrDifferent}
	}

	return nil
}

// ConditionalFilter implements ConfigFilter.
func (none NoneFilter) ConditionalFilter() string {
	return "[none]"
}

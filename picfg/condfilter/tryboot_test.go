// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package condfilter_test

import (
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/picfg/condfilter"
)

func TestTryBootFilter_MarshalText(t *testing.T) {
	var f condfilter.TryBootFilter

	text, err := f.MarshalText()
	testutil.Ok(t, err)
	testutil.Equal(t, string(text), "[tryboot]")
}

func TestTryBootFilter_UnmarshalText(t *testing.T) {
	var f condfilter.TryBootFilter

	err := f.UnmarshalText([]byte(`[tryboot]`))
	testutil.Ok(t, err)

	err = f.UnmarshalText([]byte(`[foo`))
	testutil.ErrorMatches(t, err, `TryBootFilter "\[foo" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`foo]`))
	testutil.ErrorMatches(t, err, `TryBootFilter "foo\]" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`[foo]`))
	testutil.ErrorMatches(t, err, `TryBootFilter "\[foo\]" error: different filter`)
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)
}

func TestTryBootFilter_ConditionalFilter(t *testing.T) {
	var f condfilter.TryBootFilter

	testutil.Equal(t, f.ConditionalFilter(), `[tryboot]`)
}

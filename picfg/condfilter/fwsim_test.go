// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.
// SPDX-FileCopyrightText: Zygmunt Krynicki

package condfilter_test

import (
	"strconv"
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/picfg"
	"gitlab.com/zygoon/go-raspi/picfg/condfilter"
	"gitlab.com/zygoon/go-raspi/pimodel"
)

// A number of revisions that have specific properties. Here we mostly care
// about the board type and in a specific case, about the processor used.

const (
	Cm1RevCode         = 0x0014
	Cm3PlusRevCode     = 0xa02100
	Cm3RevCode         = 0xa020a0
	Cm4RevCode         = 0xa03140
	Pi0RevCode         = 0x900092
	Pi0WRevCode        = 0x9000c1
	Pi02WRevCode       = 0x902120
	Pi1APlusRevCode    = 0x0012
	Pi1ARevCode        = 0x0007
	Pi1BPlusRevCode    = 0x0010
	Pi1BRevCode        = 0x0002
	Pi2BRevCodeBcm2836 = 0xa01040
	Pi2BRevCodeBcm2837 = 0xa02042
	Pi3APlusRevCode    = 0x9020e0
	Pi3BPlusRevCode    = 0xa020d3
	Pi3BRevCode        = 0xa02082
	Pi400RevCode       = 0xc03130
	Pi4BRevCode        = 0xa03111
	Pi5RevCode         = 0xd04170
	InvalidRevCode     = 0x000000
)

const sampleSerial = 0x10000000aa586ea0

// simulatorEnv implements condfilter.SimulatorEnvironment.
type simulatorEnv struct {
	TryBoot     bool            // Current value of tryboot flag in SoC memory.
	GpioPins    map[uint]bool   // Map from GPIO pin number to low/high state.
	EDIDs       map[uint]string // Map from display index to EDID string
	DisplayPort uint            // Index of the display probed for settings.
}

func (env *simulatorEnv) TryBootFlag() bool {
	return env.TryBoot
}

func (env *simulatorEnv) IsGpioPinHigh(pinNumber uint) bool {
	return env.GpioPins[pinNumber]
}

func (env *simulatorEnv) DisplayEdid(displayNumber uint) string {
	return env.EDIDs[displayNumber]
}

func (env *simulatorEnv) DisplayPortNumber() uint {
	return env.DisplayPort
}

func TestNewSimulator(t *testing.T) {
	t.Run("supported", func(t *testing.T) {
		t.Run("all", func(t *testing.T) {
			cfg := &picfg.ConfigTxt{Sections: []picfg.Section{
				{Filter: "[all]", Location: picfg.Location{FileName: "config.txt", LineNo: 1}}},
			}
			_, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
			testutil.Ok(t, err)
		})

		t.Run("none", func(t *testing.T) {
			cfg := &picfg.ConfigTxt{Sections: []picfg.Section{
				{Filter: "[none]", Location: picfg.Location{FileName: "config.txt", LineNo: 1}}},
			}
			_, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
			testutil.Ok(t, err)
		})

		t.Run("model", func(t *testing.T) {
			cfg := &picfg.ConfigTxt{Sections: []picfg.Section{
				{Filter: "[pi4]", Location: picfg.Location{FileName: "config.txt", LineNo: 1}}},
			}
			_, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
			testutil.Ok(t, err)
		})

		t.Run("serial", func(t *testing.T) {
			cfg := &picfg.ConfigTxt{Sections: []picfg.Section{
				{Filter: "[0x1234]", Location: picfg.Location{FileName: "config.txt", LineNo: 1}}},
			}
			_, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
			testutil.Ok(t, err)
		})

		t.Run("board-type", func(t *testing.T) {
			cfg := &picfg.ConfigTxt{Sections: []picfg.Section{
				{Filter: "[board-type=0x12]", Location: picfg.Location{FileName: "config.txt", LineNo: 1}}},
			}
			_, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
			testutil.Ok(t, err)
		})
	})

	t.Run("unsupported", func(t *testing.T) {
		t.Run("hdmi", func(t *testing.T) {
			// HDMI filter requires usage of NewDynamicSimulator.
			cfg := &picfg.ConfigTxt{Sections: []picfg.Section{
				{Filter: "[hdmi:1]", Location: picfg.Location{FileName: "config.txt", LineNo: 1}}},
			}
			_, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
			testutil.ErrorMatches(t, err, `conditional filter \[hdmi:1\] \(from config.txt:1\) is not supported`)
		})

		t.Run("edid", func(t *testing.T) {
			// EDID filter requires usage of NewDynamicSimulator.
			cfg := &picfg.ConfigTxt{Sections: []picfg.Section{
				{Filter: "[EDID=VSC-TD2220]", Location: picfg.Location{FileName: "config.txt", LineNo: 1}}},
			}
			_, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
			testutil.ErrorMatches(t, err, `conditional filter \[EDID=VSC-TD2220\] \(from config.txt:1\) is not supported`)
		})

		t.Run("gpio", func(t *testing.T) {
			// GPIO filter requires usage of NewDynamicSimulator.
			cfg := &picfg.ConfigTxt{Sections: []picfg.Section{
				{Filter: "[gpio12=1]", Location: picfg.Location{FileName: "config.txt", LineNo: 1}}},
			}
			_, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
			testutil.ErrorMatches(t, err, `conditional filter \[gpio12=1\] \(from config.txt:1\) is not supported`)
		})

		t.Run("tryboot", func(t *testing.T) {
			// Tryboot filter requires usage of NewDynamicSimulator.
			cfg := &picfg.ConfigTxt{Sections: []picfg.Section{
				{Filter: "[tryboot]", Location: picfg.Location{FileName: "config.txt", LineNo: 1}}},
			}
			_, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
			testutil.ErrorMatches(t, err, `conditional filter \[tryboot] \(from config.txt:1\) is not supported`)
		})
	})

	t.Run("invalid-filter", func(t *testing.T) {
		cfg := &picfg.ConfigTxt{Sections: []picfg.Section{
			{Filter: "[potato]", Location: picfg.Location{FileName: "config.txt", LineNo: 1}}},
		}
		_, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
		testutil.ErrorMatches(t, err, `conditional filter error "\[potato\]": unknown filter`)
	})

	t.Run("invalid-revision", func(t *testing.T) {
		cfg := &picfg.ConfigTxt{}
		_, err := condfilter.NewSimulator(cfg, 0, sampleSerial)
		testutil.ErrorMatches(t, err, `cannot decode revision code 0000, unknown board type`)
	})
}

func TestNewDynamicSimulator(t *testing.T) {
	t.Run("also-supported", func(t *testing.T) {
		var env simulatorEnv
		t.Run("hdmi", func(t *testing.T) {
			cfg := &picfg.ConfigTxt{Sections: []picfg.Section{
				{Filter: "[hdmi:1]", Location: picfg.Location{FileName: "config.txt", LineNo: 1}}},
			}
			_, err := condfilter.NewDynamicSimulator(cfg, Pi4BRevCode, sampleSerial, &env)
			testutil.Ok(t, err)
		})

		t.Run("edid", func(t *testing.T) {
			cfg := &picfg.ConfigTxt{Sections: []picfg.Section{
				{Filter: "[EDID=VSC-TD2220]", Location: picfg.Location{FileName: "config.txt", LineNo: 1}}},
			}
			_, err := condfilter.NewDynamicSimulator(cfg, Pi4BRevCode, sampleSerial, &env)
			testutil.Ok(t, err)
		})

		t.Run("gpio", func(t *testing.T) {
			cfg := &picfg.ConfigTxt{Sections: []picfg.Section{
				{Filter: "[gpio12=1]", Location: picfg.Location{FileName: "config.txt", LineNo: 1}}},
			}
			_, err := condfilter.NewDynamicSimulator(cfg, Pi4BRevCode, sampleSerial, &env)
			testutil.Ok(t, err)
		})

		t.Run("tryboot", func(t *testing.T) {
			cfg := &picfg.ConfigTxt{Sections: []picfg.Section{
				{Filter: "[tryboot]", Location: picfg.Location{FileName: "config.txt", LineNo: 1}}},
			}
			_, err := condfilter.NewDynamicSimulator(cfg, Pi4BRevCode, sampleSerial, &env)
			testutil.Ok(t, err)
		})
	})
}

func TestSimulator_Parameter(t *testing.T) {
	t.Run("complex-config", func(t *testing.T) {
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Filter:     "[all]",
				Parameters: []picfg.Parameter{{Name: "cmdline", Value: "all.txt"}},
			}, {
				Filter:     "[pi3]",
				Parameters: []picfg.Parameter{{Name: "cmdline", Value: "pi3.txt"}},
			}, {
				Filter:     "[pi4]",
				Parameters: []picfg.Parameter{{Name: "cmdline", Value: "pi4.txt"}},
			}, {
				Filter:     "[none]",
				Parameters: []picfg.Parameter{{Name: "cmdline", Value: "none.txt"}},
			}, {
				Filter:     "[board-type=0x12]",
				Parameters: []picfg.Parameter{{Name: "cmdline", Value: "pi02w.txt"}},
			}},
		}

		for i, tc := range []struct {
			code  pimodel.RevisionCode
			value string
		}{
			{code: Pi1BRevCode, value: "all.txt"},
			{code: Pi2BRevCodeBcm2836, value: "all.txt"},
			{code: Pi4BRevCode, value: "pi4.txt"},
			{code: Pi3BRevCode, value: "pi3.txt"},
			{code: Pi02WRevCode, value: "pi02w.txt"},
		} {
			t.Run(strconv.Itoa(i), func(t *testing.T) {
				sim, err := condfilter.NewSimulator(cfg, tc.code, sampleSerial)
				testutil.Ok(t, err)

				param := sim.Parameter("cmdline")
				testutil.NotNil[picfg.Parameter](t, param)
				testutil.Equal(t, param.Value, tc.value)
			})
		}
	})

	t.Run("lookup", func(t *testing.T) {
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Parameters: []picfg.Parameter{
					{Name: "param", Value: "value-1"},
					{Name: "other", Value: "value-2"},
				},
			}},
		}

		sim, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
		testutil.Ok(t, err)

		param := sim.Parameter("param")
		testutil.Equal(t, param.Name, "param")
		testutil.Equal(t, param.Value, "value-1")

		param = sim.Parameter("other")
		testutil.Equal(t, param.Name, "other")
		testutil.Equal(t, param.Value, "value-2")

		param = sim.Parameter("missing")
		testutil.Equal(t, param, nil)
	})

	t.Run("model-match", func(t *testing.T) {
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Filter:     "[pi4]",
				Parameters: []picfg.Parameter{{Name: "param", Value: "value"}},
			}},
		}

		t.Run("matching", func(t *testing.T) {
			sim, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
			testutil.Ok(t, err)

			param := sim.Parameter("param")
			testutil.Equal(t, param.Name, "param")
			testutil.Equal(t, param.Value, "value")
		})

		t.Run("non-matching", func(t *testing.T) {
			sim, err := condfilter.NewSimulator(cfg, Pi3BRevCode, sampleSerial) // non-matching case.
			testutil.Ok(t, err)

			param := sim.Parameter("param")
			testutil.Equal(t, param, nil)
		})
	})

	t.Run("serial-match", func(t *testing.T) {
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Filter:     "[0x1234]",
				Parameters: []picfg.Parameter{{Name: "param", Value: "value"}},
			}},
		}

		t.Run("matching", func(t *testing.T) {
			sim, err := condfilter.NewSimulator(cfg, Pi4BRevCode, 0x1234)
			testutil.Ok(t, err)

			param := sim.Parameter("param")
			testutil.Equal(t, param.Name, "param")
			testutil.Equal(t, param.Value, "value")
		})

		t.Run("non-matching", func(t *testing.T) {
			sim, err := condfilter.NewSimulator(cfg, Pi4BRevCode, 0x5678) // non-matching case.
			testutil.Ok(t, err)

			param := sim.Parameter("param")
			testutil.Equal(t, param, nil)
		})
	})

	t.Run("tryboot-match", func(t *testing.T) {
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Filter:     "[tryboot]",
				Parameters: []picfg.Parameter{{Name: "param", Value: "value"}},
			}},
		}

		t.Run("matching", func(t *testing.T) {
			env := simulatorEnv{TryBoot: true}
			sim, err := condfilter.NewDynamicSimulator(cfg, Pi4BRevCode, 0x1234, &env)
			testutil.Ok(t, err)

			param := sim.Parameter("param")
			testutil.Equal(t, param.Name, "param")
			testutil.Equal(t, param.Value, "value")
		})

		t.Run("non-matching", func(t *testing.T) {
			env := simulatorEnv{TryBoot: false}
			sim, err := condfilter.NewDynamicSimulator(cfg, Pi4BRevCode, 0x1234, &env)
			testutil.Ok(t, err)

			param := sim.Parameter("param")
			testutil.Equal(t, param, nil)
		})
	})

	t.Run("hdmi-match", func(t *testing.T) {
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Filter:     "[hdmi:1]",
				Parameters: []picfg.Parameter{{Name: "param", Value: "value"}},
			}},
		}

		t.Run("matching", func(t *testing.T) {
			env := simulatorEnv{DisplayPort: 1}
			sim, err := condfilter.NewDynamicSimulator(cfg, Pi4BRevCode, 0x1234, &env)
			testutil.Ok(t, err)

			param := sim.Parameter("param")
			testutil.Equal(t, param.Name, "param")
			testutil.Equal(t, param.Value, "value")
		})

		t.Run("non-matching", func(t *testing.T) {
			env := simulatorEnv{}
			sim, err := condfilter.NewDynamicSimulator(cfg, Pi4BRevCode, 0x1234, &env)
			testutil.Ok(t, err)

			param := sim.Parameter("param")
			testutil.Equal(t, param, nil)
		})

		t.Run("pi5", func(t *testing.T) {
			env := simulatorEnv{DisplayPort: 1}
			sim, err := condfilter.NewDynamicSimulator(cfg, Pi5RevCode, 0x1234, &env)
			testutil.Ok(t, err)

			// Pi5 does not support [HDMI:N] filtering.
			param := sim.Parameter("param")
			testutil.Equal(t, param, nil)
		})
	})

	t.Run("edid-match", func(t *testing.T) {
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Filter:     "[EDID=potato]",
				Parameters: []picfg.Parameter{{Name: "param", Value: "value"}},
			}},
		}

		t.Run("matching-0", func(t *testing.T) {
			env := simulatorEnv{EDIDs: map[uint]string{0: "potato"}}
			sim, err := condfilter.NewDynamicSimulator(cfg, Pi4BRevCode, 0x1234, &env)
			testutil.Ok(t, err)

			param := sim.Parameter("param")
			testutil.Equal(t, param.Name, "param")
			testutil.Equal(t, param.Value, "value")
		})

		t.Run("matching-1", func(t *testing.T) {
			env := simulatorEnv{EDIDs: map[uint]string{0: "fries", 1: "potato"}}
			sim, err := condfilter.NewDynamicSimulator(cfg, Pi4BRevCode, 0x1234, &env)
			testutil.Ok(t, err)

			param := sim.Parameter("param")
			testutil.Equal(t, param.Name, "param")
			testutil.Equal(t, param.Value, "value")
		})

		t.Run("non-matching", func(t *testing.T) {
			env := simulatorEnv{}
			sim, err := condfilter.NewDynamicSimulator(cfg, Pi4BRevCode, 0x1234, &env)
			testutil.Ok(t, err)

			param := sim.Parameter("param")
			testutil.Equal(t, param, nil)
		})

		t.Run("pi5", func(t *testing.T) {
			env := simulatorEnv{EDIDs: map[uint]string{0: "potato"}}
			sim, err := condfilter.NewDynamicSimulator(cfg, Pi5RevCode, 0x1234, &env)
			testutil.Ok(t, err)

			// Pi5 does not support [EDID=*] filtering.
			param := sim.Parameter("param")
			testutil.Equal(t, param, nil)
		})

	})

	t.Run("gpio-match", func(t *testing.T) {
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Filter:     "[gpio7=1]",
				Parameters: []picfg.Parameter{{Name: "param", Value: "value"}},
			}},
		}

		t.Run("matching", func(t *testing.T) {
			env := simulatorEnv{GpioPins: map[uint]bool{7: true}}
			sim, err := condfilter.NewDynamicSimulator(cfg, Pi4BRevCode, 0x1234, &env)
			testutil.Ok(t, err)

			param := sim.Parameter("param")
			testutil.Equal(t, param.Name, "param")
			testutil.Equal(t, param.Value, "value")
		})

		t.Run("non-matching", func(t *testing.T) {
			env := simulatorEnv{GpioPins: map[uint]bool{7: false}}
			sim, err := condfilter.NewDynamicSimulator(cfg, Pi4BRevCode, 0x1234, &env)
			testutil.Ok(t, err)

			param := sim.Parameter("param")
			testutil.Equal(t, param, nil)
		})
	})
}

func TestSimulator_CoherentParameter(t *testing.T) {
	t.Run("skips-none-section", func(t *testing.T) {
		// CoherentParameter skips the [none] section and does look at anything
		// inside.
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Parameters: []picfg.Parameter{{
					Name:     "os_prefix",
					Value:    "slot-a/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 1},
				}},
			}, {
				Filter: "[none]",
				Parameters: []picfg.Parameter{{
					Name:     "os_prefix",
					Value:    "potato/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 3},
				}},
			}},
		}

		sim, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
		testutil.Ok(t, err)

		param, err := sim.CoherentParameter("os_prefix")
		testutil.Ok(t, err)
		testutil.NotNil[picfg.Parameter](t, param)
		testutil.Equal(t, param.Location.LineNo, 1)
	})

	t.Run("last-definition", func(t *testing.T) {
		// CoherentParameter picks up the last definition.
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Parameters: []picfg.Parameter{{
					Name:     "os_prefix",
					Value:    "slot-a/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 1},
				}, {
					Name:     "os_prefix",
					Value:    "slot-a/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 2},
				}},
			}},
		}

		sim, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
		testutil.Ok(t, err)

		param, err := sim.CoherentParameter("os_prefix")
		testutil.Ok(t, err)
		testutil.Equal(t, param.Value, "slot-a/")
		// Note that we pick up the second definition.
		testutil.Equal(t, param.Location, picfg.Location{FileName: "config.txt", LineNo: 2})
	})

	t.Run("non-coherent-non-matching-os-prefix", func(t *testing.T) {
		// The os_prefix= parameter should be coherent among all the sections, even
		// those not matching the current model. Here the Pi4B model matches [pi4]
		// but not the [pi3] above it.
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Filter: "[pi3]",
				Parameters: []picfg.Parameter{{
					Name:     "os_prefix",
					Value:    "potato/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 2},
				}},
			}, {
				Filter: "[pi4]",
				Parameters: []picfg.Parameter{{
					Name:     "os_prefix",
					Value:    "tomato/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 4},
				}},
			}},
		}

		sim, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
		testutil.Ok(t, err)

		_, err = sim.CoherentParameter("os_prefix")
		testutil.ErrorMatches(t, err, `conflicting definition of os_prefix, `+
			`first defined in config.txt:2 as potato/, `+
			`then in config.txt:4 as tomato/`)
	})

	t.Run("non-coherent-matching-os-prefix", func(t *testing.T) {
		// The os_prefix= parameter should be coherent among all the sections
		// matching the current model. Here the Pi4B model matches both [pi4] and
		// [all].
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Filter: "[pi4]",
				Parameters: []picfg.Parameter{{
					Name:     "os_prefix",
					Value:    "potato/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 2},
				}},
			}, {
				Filter: "[all]",
				Parameters: []picfg.Parameter{{
					Name:     "os_prefix",
					Value:    "tomato/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 4},
				}},
			}},
		}

		sim, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
		testutil.Ok(t, err)

		_, err = sim.CoherentParameter("os_prefix")
		testutil.ErrorMatches(t, err, `conflicting definition of os_prefix, `+
			`first defined in config.txt:2 as potato/, `+
			`then in config.txt:4 as tomato/`)
	})

	t.Run("lookup", func(t *testing.T) {
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Parameters: []picfg.Parameter{
					{Name: "param", Value: "value-1"},
					{Name: "other", Value: "value-2"},
				},
			}},
		}

		sim, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
		testutil.Ok(t, err)

		param, err := sim.CoherentParameter("param")
		testutil.Ok(t, err)
		testutil.Equal(t, param.Name, "param")
		testutil.Equal(t, param.Value, "value-1")

		param, err = sim.CoherentParameter("other")
		testutil.Ok(t, err)
		testutil.Equal(t, param.Name, "other")
		testutil.Equal(t, param.Value, "value-2")

		param, err = sim.CoherentParameter("missing")
		testutil.Ok(t, err)
		testutil.Equal(t, param, nil)
	})
}

func TestSimulator_SetCoherentParameter(t *testing.T) {
	t.Run("skips-none-section", func(t *testing.T) {
		// SetCoherentParameter skips the [none] section and does not update
		// anything inside.
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Parameters: []picfg.Parameter{{
					Name:     "os_prefix",
					Value:    "slot-a/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 1},
				}},
			}, {
				Filter: "[none]",
				Parameters: []picfg.Parameter{{
					Name:     "os_prefix",
					Value:    "potato/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 3},
				}},
			}},
		}

		sim, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
		testutil.Ok(t, err)

		err = sim.SetCoherentParameter("os_prefix", "slot-b/")
		testutil.Ok(t, err)

		testutil.ObjectEqual(t, cfg, &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Parameters: []picfg.Parameter{{
					Name:     "os_prefix",
					Value:    "slot-b/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 1},
				}},
			}, {
				Filter: "[none]",
				Parameters: []picfg.Parameter{{
					Name:     "os_prefix",
					Value:    "potato/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 3},
				}},
			}},
		})
	})

	t.Run("adds-all-section", func(t *testing.T) {
		// SetCoherentParameter adds a new [all] section is one is not available.
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Filter: "[pi4]",
			}},
		}

		sim, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
		testutil.Ok(t, err)

		err = sim.SetCoherentParameter("os_prefix", "slot-b/")
		testutil.Ok(t, err)

		testutil.ObjectEqual(t, cfg, &picfg.ConfigTxt{
			Sections: []picfg.Section{
				{
					Filter: "[pi4]",
				},
				{
					Filter: "[all]",
					Parameters: []picfg.Parameter{{
						Name:  "os_prefix",
						Value: "slot-b/",
					}},
				},
			},
		})
	})

	t.Run("refuses-to-clobber-non-coherent-values", func(t *testing.T) {
		// SetCoherentParameter refuses to modify a non-coherent parameter.
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Filter: "[pi4]",
				Parameters: []picfg.Parameter{{
					Name:     "os_prefix",
					Value:    "potato/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 2},
				}},
			}, {
				Filter: "[all]",
				Parameters: []picfg.Parameter{{
					Name:     "os_prefix",
					Value:    "tomato/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 4},
				}},
			}},
		}

		sim, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
		testutil.Ok(t, err)

		err = sim.SetCoherentParameter("os_prefix", "slot-b/")
		testutil.ErrorMatches(t, err, `conflicting definition of os_prefix, first defined in config.txt:2 as potato/, then in config.txt:4 as tomato/`)
	})

	t.Run("updates-all-instances", func(t *testing.T) {
		// SetCoherentParameter updates all the definitions.
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Parameters: []picfg.Parameter{{
					Name:     "os_prefix",
					Value:    "slot-a/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 1},
				}, {
					Name:     "os_prefix",
					Value:    "slot-a/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 2},
				}},
			}},
		}

		sim, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
		testutil.Ok(t, err)

		err = sim.SetCoherentParameter("os_prefix", "slot-b/")
		testutil.Ok(t, err)

		testutil.ObjectEqual(t, cfg, &picfg.ConfigTxt{
			Sections: []picfg.Section{{
				Parameters: []picfg.Parameter{{
					Name:     "os_prefix",
					Value:    "slot-b/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 1},
				}, {
					Name:     "os_prefix",
					Value:    "slot-b/",
					Location: picfg.Location{FileName: "config.txt", LineNo: 2},
				}},
			}},
		})
	})

	t.Run("uses-last-all-section", func(t *testing.T) {
		// SetCoherentParameter updates all the definitions.
		cfg := &picfg.ConfigTxt{
			Sections: []picfg.Section{
				{},                // Implicit [all] section.
				{Filter: "[all]"}, // First explicit [all] section.
				{Filter: "[all]"}, // Second explicit [all] section
			},
		}

		sim, err := condfilter.NewSimulator(cfg, Pi4BRevCode, sampleSerial)
		testutil.Ok(t, err)

		err = sim.SetCoherentParameter("os_prefix", "slot-a/")
		testutil.Ok(t, err)

		testutil.ObjectEqual(t, cfg, &picfg.ConfigTxt{
			Sections: []picfg.Section{
				{},
				{Filter: "[all]"},
				{Filter: "[all]", Parameters: []picfg.Parameter{{Name: "os_prefix", Value: "slot-a/"}}},
			},
		})
	})
}

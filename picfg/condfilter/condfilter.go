// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package condfilter implements conditional filters as distinct types.
//
// The Raspberry Pi boot configuration file defines a number of conditional
// filters which impact how subsequent options are interpreted by the firmware.
// This system is stateful and distinct types concatenate while similar types
// override each other.
//
// Conditional filters are documented at
// https://www.raspberrypi.org/documentation/configuration/config-txt/conditional.md
package condfilter

import (
	"bytes"
	"errors"
	"fmt"
	"strconv"
)

var (
	// ErrSyntax reports filters with incorrect syntax, as indicated by lack of square brackets.
	ErrSyntax = errors.New("syntax error")
	// ErrParameterRequired reports filters with missing parameters, such as HDMI port number or GPIO pin.
	ErrParameterRequired = errors.New("parameter value required")
	// ErrDifferent reports discovering filter different from what was expected.
	ErrDifferent = errors.New("different filter")
	// ErrUnknown reports syntactically valid, unknown filter types.
	ErrUnknown = errors.New("unknown filter")
)

// FilterError reports problem with recognizing a conditional filter.
//
// This error is returned from the UnmarshalText methods of individual filter
// types, as well as from the Unmarshal function defined in the package.
type FilterError struct {
	Type   string // the failing type (AllFilter, EDIDFilter, GPIOFilter, HDMIFilter, ModelFilter, NoneFilter, SerialFilter, TryBootFilter), if a specific filter was parsed.
	Filter string // the input
	Err    error  // the reason the conversion failed (ErrSyntax, ErrDifferent, ...)
}

// Error implements the error interface.
func (e *FilterError) Error() string {
	if e.Type != "" {
		return fmt.Sprintf("%s %q error: %v", e.Type, e.Filter, e.Err)
	}

	return fmt.Sprintf("conditional filter error %q: %v", e.Filter, e.Err)
}

// Unwrap returns the error wrapped inside FilterError.
func (e *FilterError) Unwrap() error {
	return e.Err
}

// ConditionalFilter constrains subsequent configuration parameters.
type ConditionalFilter interface {
	// ConditionalFilter returns the textual representation of the filter.
	ConditionalFilter() string
}

// MustUnmarshal recognizes any valid conditional filter or panics.
func MustUnmarshal(text []byte) ConditionalFilter {
	filter, err := Unmarshal(text)
	if err != nil {
		panic(err)
	}

	return filter
}

// Unmarshal recognizes any valid conditional filter.
//
// If there is an error it will be of type *UnknownConditionalFilter.
func Unmarshal(text []byte) (ConditionalFilter, error) {
	// Does it look like a filter?
	if _, err := parseName(text, ""); err != nil {
		return nil, err
	}

	// Is it any of the known filters?

	var modelFilter PiModelFilter

	err := modelFilter.UnmarshalText(text)
	if err == nil {
		return modelFilter, nil
	}

	if !errors.Is(err, ErrDifferent) {
		return nil, err
	}

	var edidFilter EDIDFilter

	err = edidFilter.UnmarshalText(text)
	if err == nil {
		return edidFilter, nil
	}

	if !errors.Is(err, ErrDifferent) {
		return nil, err
	}

	var gpioFilter GPIOFilter

	err = gpioFilter.UnmarshalText(text)
	if err == nil {
		return gpioFilter, nil
	}

	if !errors.Is(err, ErrDifferent) {
		return nil, err
	}

	var serialFilter SerialFilter

	err = serialFilter.UnmarshalText(text)
	if err == nil {
		return serialFilter, nil
	}

	if !errors.Is(err, ErrDifferent) {
		return nil, err
	}

	var hdmiFilter HDMIFilter

	err = hdmiFilter.UnmarshalText(text)
	if err == nil {
		return hdmiFilter, nil
	}

	if !errors.Is(err, ErrDifferent) {
		return nil, err
	}

	var boardTypeFilter BoardTypeFilter

	err = boardTypeFilter.UnmarshalText(text)
	if err == nil {
		return boardTypeFilter, nil
	}

	if !errors.Is(err, ErrDifferent) {
		return nil, err
	}

	var allFilter AllFilter

	err = allFilter.UnmarshalText(text)
	if err == nil {
		return allFilter, nil
	}

	if !errors.Is(err, ErrDifferent) {
		return nil, err
	}

	var noneFilter NoneFilter

	err = noneFilter.UnmarshalText(text)
	if err == nil {
		return noneFilter, nil
	}

	if !errors.Is(err, ErrDifferent) {
		return nil, err
	}

	var tryBootFilter TryBootFilter

	err = tryBootFilter.UnmarshalText(text)
	if err == nil {
		return tryBootFilter, nil
	}

	if !errors.Is(err, ErrDifferent) {
		return nil, err
	}

	return nil, &FilterError{Filter: string(text), Err: ErrUnknown}
}

// Parameter identifies the particular parameter of a conditional filter.
type Parameter int

const (
	// GPIOPinNumber designates the number of GPIO pin of GPIOFilter.
	GPIOPinNumber Parameter = iota
	// GPIOPinState designates the state of a GPIO pin of GPIOFilter.
	GPIOPinState
	// HDMIPortNumber designates the number of an HDMI port of HDMIFilter.
	HDMIPortNumber
	// CPUSerialNumber designates the CPU serial number of a SerialFilter.
	CPUSerialNumber
	// MonitorEDID designates the EDID of a EDIDFilter.
	MonitorEDID
	// BoardTypeNumber designates the board type code of BoardTypeFilter
	BoardTypeNumber
)

// String returns the name of the parameter and is useful in error messages.
func (n Parameter) String() string {
	switch n {
	case GPIOPinNumber:
		return "GPIO pin number"
	case GPIOPinState:
		return "GPIO pin state"
	case HDMIPortNumber:
		return "HDMI port number"
	case CPUSerialNumber:
		return "CPU serial number"
	case MonitorEDID:
		return "monitor EDID"
	case BoardTypeNumber:
		return "board type number"
	default:
		return fmt.Sprintf("Parameter(%d)", int(n))
	}
}

// ParameterError records problem with a parameter of a specific filter.
type ParameterError struct {
	What Parameter
	Err  error
}

// Error implements the error interface.
//
// Extra care is taken to avoid formatting strconv.NumError which has ugly error
// messages. Those are unpacked to show the underlying error, omitting the
// details of the formatting function.
func (e *ParameterError) Error() string {
	var numErr *strconv.NumError

	// Unpack ugly strconv.NumError and get the root of the problem out.
	wrappedErr := e.Err
	if errors.As(e.Err, &numErr) {
		wrappedErr = numErr.Err
	}

	return fmt.Sprintf("invalid %s: %v", e.What, wrappedErr)
}

// Unwrap returns the error wrapped inside the invalid numeric property error.
func (e *ParameterError) Unwrap() error {
	return e.Err
}

func parseName(text []byte, typ string) (string, error) {
	if !bytes.HasPrefix(text, []byte("[")) || !bytes.HasSuffix(text, []byte("]")) {
		return "", &FilterError{Type: typ, Filter: string(text), Err: ErrSyntax}
	}

	return string(text[1 : len(text)-1]), nil
}

func parseNameParam(text []byte, typ string, valueSep rune) (string, string, error) {
	if !bytes.HasPrefix(text, []byte("[")) || !bytes.HasSuffix(text, []byte("]")) {
		return "", "", &FilterError{Type: typ, Filter: string(text), Err: ErrSyntax}
	}

	idx := bytes.IndexRune(text, valueSep)
	if idx == -1 {
		return string(text[1 : len(text)-1]), "", nil
	}

	return string(text[1:idx]), string(text[idx+1 : len(text)-1]), nil
}

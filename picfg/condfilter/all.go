// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter

// AllFilter is a config.txt conditional filter lifting all prior restrictions.
type AllFilter struct{}

// All is an instance of AllFilter.
var All = AllFilter{}

// MarshalText implements TextMarshaler.
func (all AllFilter) MarshalText() ([]byte, error) {
	return []byte(all.ConditionalFilter()), nil
}

// UnmarshalText implements TextUnmarshaler.
func (all *AllFilter) UnmarshalText(text []byte) error {
	const typ = "AllFilter"

	name, err := parseName(text, typ)
	if err != nil {
		return err
	}

	if name != "all" {
		return &FilterError{Type: typ, Filter: string(text), Err: ErrDifferent}
	}

	return nil
}

// ConditionalFilter implements ConditionalFilter.
func (all AllFilter) ConditionalFilter() string {
	return "[all]"
}

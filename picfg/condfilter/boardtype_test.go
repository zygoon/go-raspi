// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter_test

import (
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/picfg/condfilter"
	"gitlab.com/zygoon/go-raspi/pimodel"
)

func TestBoardTypeFilter_MarshalText(t *testing.T) {
	text, err := condfilter.BoardTypeFilter(0x12).MarshalText()
	testutil.Ok(t, err)
	testutil.Equal(t, string(text), "[board-type=0x12]")
}

func TestBoardTypeFilter_UnmarshalText(t *testing.T) {
	var f condfilter.BoardTypeFilter

	// Hexadecimal values are recognized.
	err := f.UnmarshalText([]byte(`[board-type=0x12]`))
	testutil.Ok(t, err)
	testutil.Equal(t, f, condfilter.BoardTypeFilter(0x12))

	// Decimal values are recognized.
	err = f.UnmarshalText([]byte(`[board-type=12]`))
	testutil.Ok(t, err)
	testutil.Equal(t, f, condfilter.BoardTypeFilter(12))

	// Range checking is in effect.
	err = f.UnmarshalText([]byte(`[board-type=-1]`))
	testutil.ErrorMatches(t, err, `BoardTypeFilter "\[board-type=-1\]" error: invalid board type number: invalid syntax`)

	err = f.UnmarshalText([]byte(`[board-type=256]`))
	testutil.ErrorMatches(t, err, `BoardTypeFilter "\[board-type=256\]" error: invalid board type number: value out of range`)

	// Valid range is 0-255 inclusive.
	err = f.UnmarshalText([]byte(`[board-type=0]`))
	testutil.Ok(t, err)
	testutil.Equal(t, f, condfilter.BoardTypeFilter(0))

	err = f.UnmarshalText([]byte(`[board-type=255]`))
	testutil.Ok(t, err)
	testutil.Equal(t, f, condfilter.BoardTypeFilter(255))

	err = f.UnmarshalText([]byte(`[board-type=]`))
	testutil.ErrorMatches(t, err, `BoardTypeFilter "\[board-type=\]" error: invalid board type number: parameter value required`)
	testutil.ErrorIs(t, err, condfilter.ErrParameterRequired)
	paramErr := testutil.ErrorAs[*condfilter.ParameterError](t, err)
	testutil.Equal(t, paramErr.What, condfilter.BoardTypeNumber)

	err = f.UnmarshalText([]byte(`[board-type]`))
	testutil.ErrorMatches(t, err, `BoardTypeFilter "\[board-type\]" error: different filter`)
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)

	err = f.UnmarshalText([]byte(`[foo`))
	testutil.ErrorMatches(t, err, `BoardTypeFilter "\[foo" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`foo]`))
	testutil.ErrorMatches(t, err, `BoardTypeFilter "foo\]" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`[foo]`))
	testutil.ErrorMatches(t, err, `BoardTypeFilter "\[foo\]" error: different filter`)
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)

	err = f.UnmarshalText([]byte(`[foo=]`))
	testutil.ErrorMatches(t, err, `BoardTypeFilter "\[foo=\]" error: different filter`)
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)
}

func TestBoardTypeFilter_ConditionalFilter(t *testing.T) {
	testutil.Equal(t, condfilter.BoardTypeFilter(0x12).ConditionalFilter(), `[board-type=0x12]`)
}

func TestBoardTypeFilter_MatchesRevCode(t *testing.T) {
	testutil.Equal(t, condfilter.BoardTypeFilter(0x14).MatchesRevCode(Cm4RevCode), true)
	testutil.Equal(t, condfilter.BoardTypeFilter(0x00).MatchesRevCode(Cm4RevCode), false)

	// Cm1RevCode is from the old revision code family.
	testutil.Equal(t, condfilter.BoardTypeFilter(0x06).MatchesRevCode(Cm1RevCode), true)
	testutil.Equal(t, condfilter.BoardTypeFilter(0x00).MatchesRevCode(Cm1RevCode), false)

	_, err := pimodel.RevisionCode(InvalidRevCode).BoardTypeCode()
	testutil.NotOk(t, err)

	testutil.Equal(t, condfilter.BoardTypeFilter(0x00).MatchesRevCode(InvalidRevCode), false)
}

// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter

import (
	"bytes"
	"fmt"
)

// EDIDFilter restricts subsequent boot options to a devices having a monitor with a given EDID.
type EDIDFilter string

// MarshalText implements TextMarshaler.
func (edid EDIDFilter) MarshalText() ([]byte, error) {
	return []byte(edid.ConditionalFilter()), nil
}

// UnmarshalText implements TextUnmarshaler.
func (edid *EDIDFilter) UnmarshalText(text []byte) error {
	const typ = "EDIDFilter"

	name, param, err := parseNameParam(text, typ, '=')
	if err != nil {
		return err
	}

	if name != "EDID" {
		// [EDID] is an altogether different filter.
		return &FilterError{Type: typ, Filter: string(text), Err: ErrDifferent}
	}

	if param == "" {
		if !bytes.ContainsRune(text, '=') {
			// [EDID] is an altogether different filter.
			return &FilterError{Type: typ, Filter: string(text), Err: ErrDifferent}
		}
		// [EDID=] is an invalid use of the [EDID:foo] filter.
		return &FilterError{Type: typ, Filter: string(text), Err: &ParameterError{What: MonitorEDID, Err: ErrParameterRequired}}
	}

	*edid = EDIDFilter(param)

	return nil
}

// ConditionalFilter implements ConditionalFilter.
func (edid EDIDFilter) ConditionalFilter() string {
	return fmt.Sprintf("[EDID=%s]", string(edid))
}

// MonitorEDID returns the expected EDID string.
func (edid EDIDFilter) MonitorEDID() string {
	return string(edid)
}

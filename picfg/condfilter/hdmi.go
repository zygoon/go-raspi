// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter

import (
	"bytes"
	"fmt"
	"strconv"
)

// HDMIFilter restricts subsequent configuration options to a specific HDMI port.
//
// The syntax of the filter inside a config.txt file is [hdmi:N], where N is a
// decimal encoding the specific HDMI port.
type HDMIFilter struct {
	PortNumber uint
}

// MarshalText implements TextMarshaler.
func (hdmi HDMIFilter) MarshalText() ([]byte, error) {
	return []byte(hdmi.ConditionalFilter()), nil
}

// UnmarshalText implements TextUnmarshaler.
func (hdmi *HDMIFilter) UnmarshalText(text []byte) error {
	const typ = "HDMIFilter"

	name, param, err := parseNameParam(text, typ, ':')
	if err != nil {
		return err
	}

	if name != "hdmi" {
		return &FilterError{Type: typ, Filter: string(text), Err: ErrDifferent}
	}

	if param == "" {
		if !bytes.ContainsRune(text, ':') {
			// [hdmi] is an altogether different filter.
			return &FilterError{Type: typ, Filter: string(text), Err: ErrDifferent}
		}
		// [hdmi:] is an invalid use of the [hdmi:N] filter.
		return &FilterError{Type: typ, Filter: string(text), Err: &ParameterError{What: HDMIPortNumber, Err: ErrParameterRequired}}
	}

	portNumber, err := strconv.ParseUint(param, 10, 16)
	if err != nil {
		return &FilterError{Type: typ, Filter: string(text), Err: &ParameterError{What: HDMIPortNumber, Err: err}}
	}

	hdmi.PortNumber = uint(portNumber)

	return nil
}

// ConditionalFilter implements ConditionalFilter.
func (hdmi HDMIFilter) ConditionalFilter() string {
	return fmt.Sprintf("[hdmi:%d]", hdmi.PortNumber)
}

// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.
// SPDX-FileCopyrightText: Zygmunt Krynicki

package condfilter_test

import (
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/picfg/condfilter"
)

func TestStateTracker_EffectiveFilters(t *testing.T) {
	var st condfilter.StateTracker

	// When there are no conditional filters, it's equivalent to the [all] filter.
	testutil.SlicesEqual(t, st.EffectiveFilters(), []condfilter.ConditionalFilter{condfilter.All})

	// Conditional filters of the same type override each other
	st.Filter(condfilter.Pi0)
	testutil.SlicesEqual(t, st.EffectiveFilters(), []condfilter.ConditionalFilter{condfilter.Pi0})
	st.Filter(condfilter.Pi1)
	testutil.SlicesEqual(t, st.EffectiveFilters(), []condfilter.ConditionalFilter{condfilter.Pi1})
	st.Filter(condfilter.Pi2)
	testutil.SlicesEqual(t, st.EffectiveFilters(), []condfilter.ConditionalFilter{condfilter.Pi2})

	// Model and board-type override each other.
	st.Filter(condfilter.BoardTypeFilter(0x12))
	st.Filter(condfilter.Pi02)
	testutil.SlicesEqual(t, st.EffectiveFilters(), []condfilter.ConditionalFilter{condfilter.Pi02})

	st.Filter(condfilter.Pi02)
	st.Filter(condfilter.BoardTypeFilter(0x12))
	testutil.SlicesEqual(t, st.EffectiveFilters(), []condfilter.ConditionalFilter{condfilter.BoardTypeFilter(0x012)})

	// Unrelated filters stack. There are separate types for: model-or-board-type, edid, hdmi, serial and gpio
	st.Filter(condfilter.Pi4)
	st.Filter(condfilter.EDIDFilter("foo"))
	st.Filter(condfilter.HDMIFilter{PortNumber: 1})
	st.Filter(condfilter.SerialFilter(0x1234))
	st.Filter(condfilter.GPIOFilter{PinNumber: 4, High: true})
	st.Filter(condfilter.TryBoot)

	// Order of effective filters is fixed, regardless of order in which filters are presented.
	testutil.SlicesEqual(t, st.EffectiveFilters(), []condfilter.ConditionalFilter{
		condfilter.Pi4,
		condfilter.EDIDFilter("foo"),
		condfilter.GPIOFilter{PinNumber: 4, High: true},
		condfilter.SerialFilter(0x1234),
		condfilter.HDMIFilter{PortNumber: 1},
		condfilter.TryBoot,
	})
	testutil.Equal(t, st.IsNone(), false)
	testutil.Equal(t, st.IsAll(), false)

	// The [none] filter overrides all others.
	st.Filter(condfilter.None)
	testutil.SlicesEqual(t, st.EffectiveFilters(), []condfilter.ConditionalFilter{condfilter.None})

	testutil.Equal(t, st.IsNone(), true)
	testutil.Equal(t, st.IsAll(), false)

	// The [all] filter resets all previous filters
	st.Filter(condfilter.All)
	testutil.SlicesEqual(t, st.EffectiveFilters(), []condfilter.ConditionalFilter{condfilter.All})

	testutil.Equal(t, st.IsNone(), false)
	testutil.Equal(t, st.IsAll(), true)
}

func TestStateTracker_AllFilter(t *testing.T) {
	var st condfilter.StateTracker

	f := condfilter.All
	st.Filter(&f)
	st.Filter(f)

	testutil.Equal(t, st.IsNone(), false)
	testutil.Equal(t, st.IsAll(), true)
}

func TestStateTracker_NoneFilter(t *testing.T) {
	var st condfilter.StateTracker

	f := condfilter.None
	st.Filter(&f)
	st.Filter(f)

	testutil.Equal(t, st.IsNone(), true)
	testutil.Equal(t, st.IsAll(), false)
}

func TestStateTracker_PiModelFilter(t *testing.T) {
	var st condfilter.StateTracker

	f := condfilter.Pi0
	st.Filter(&f)
	st.Filter(f)

	testutil.Equal(t, st.IsNone(), false)
	testutil.Equal(t, st.IsAll(), false)
}

func TestStateTracker_SerialFilter(t *testing.T) {
	var st condfilter.StateTracker

	f := condfilter.SerialFilter(0x1234)
	st.Filter(&f)
	st.Filter(f)

	testutil.Equal(t, st.IsNone(), false)
	testutil.Equal(t, st.IsAll(), false)
}

func TestStateTracker_GPIOFilter(t *testing.T) {
	var st condfilter.StateTracker

	f := condfilter.GPIOFilter{PinNumber: 1, High: true}
	st.Filter(&f)
	st.Filter(f)

	testutil.Equal(t, st.IsNone(), false)
	testutil.Equal(t, st.IsAll(), false)
}

func TestStateTracker_EDIDFilter(t *testing.T) {
	var st condfilter.StateTracker

	f := condfilter.EDIDFilter("foo")
	st.Filter(&f)
	st.Filter(f)

	testutil.Equal(t, st.IsNone(), false)
	testutil.Equal(t, st.IsAll(), false)
}

func TestStateTracker_HDMIFilter(t *testing.T) {
	var st condfilter.StateTracker

	f := condfilter.HDMIFilter{PortNumber: 1}
	st.Filter(&f)
	st.Filter(f)

	testutil.Equal(t, st.IsNone(), false)
	testutil.Equal(t, st.IsAll(), false)
}

func TestStateTracker_TryBoot(t *testing.T) {
	var st condfilter.StateTracker

	f := condfilter.TryBootFilter{}
	st.Filter(&f)
	st.Filter(f)

	testutil.Equal(t, st.IsNone(), false)
	testutil.Equal(t, st.IsAll(), false)
}

func TestStateTracker_BoardTypeFilter(t *testing.T) {
	var st condfilter.StateTracker

	f := condfilter.BoardTypeFilter(0x12)
	st.Filter(&f)
	st.Filter(f)

	testutil.Equal(t, st.IsNone(), false)
	testutil.Equal(t, st.IsAll(), false)
}

type mysteryFilter struct{}

func (mysteryFilter) ConditionalFilter() string {
	return "[???]"
}

func TestStateTracker_UnknownFilter(t *testing.T) {
	var st condfilter.StateTracker

	testutil.PanicMatches(t, func() { st.Filter(mysteryFilter{}) }, "unexpected filter type: condfilter_test.mysteryFilter")
}

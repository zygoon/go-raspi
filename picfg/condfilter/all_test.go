// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter_test

import (
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/picfg/condfilter"
)

func TestAllFilter_MarshalText(t *testing.T) {
	var f condfilter.AllFilter

	text, err := f.MarshalText()
	testutil.Ok(t, err)
	testutil.Equal(t, string(text), "[all]")
}

func TestAllFilter_UnmarshalText(t *testing.T) {
	var f condfilter.AllFilter

	err := f.UnmarshalText([]byte(`[all]`))
	testutil.Ok(t, err)

	err = f.UnmarshalText([]byte(`[foo`))
	testutil.ErrorMatches(t, err, `AllFilter "\[foo" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`foo]`))
	testutil.ErrorMatches(t, err, `AllFilter "foo\]" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`[foo]`))
	testutil.ErrorMatches(t, err, `AllFilter "\[foo\]" error: different filter`)
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)
}

func TestAllFilter_ConditionalFilter(t *testing.T) {
	var f condfilter.AllFilter

	testutil.Equal(t, f.ConditionalFilter(), `[all]`)
}

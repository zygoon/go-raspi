// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter_test

import (
	"strconv"
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/picfg/condfilter"
)

func TestSerialFilter_MarshalText(t *testing.T) {
	serialFilter := condfilter.SerialFilter(0x1234)
	text, err := serialFilter.MarshalText()
	testutil.Ok(t, err)
	testutil.Equal(t, string(text), "[0x1234]")
}

func TestSerialFilter_UnmarshalText(t *testing.T) {
	var f condfilter.SerialFilter

	err := f.UnmarshalText([]byte(`[0x1234]`))
	testutil.Ok(t, err)
	testutil.Equal(t, f, condfilter.SerialFilter(0x1234))

	err = f.UnmarshalText([]byte(`[0x potato]`))
	testutil.ErrorMatches(t, err, `SerialFilter "\[0x potato\]" error: invalid CPU serial number: invalid syntax`)
	testutil.ErrorIs(t, err, strconv.ErrSyntax)
	paramErr := testutil.ErrorAs[*condfilter.ParameterError](t, err)
	testutil.Equal(t, paramErr.What, condfilter.CPUSerialNumber)

	err = f.UnmarshalText([]byte(`[foo`))
	testutil.ErrorMatches(t, err, `SerialFilter "\[foo" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`foo]`))
	testutil.ErrorMatches(t, err, `SerialFilter "foo\]" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`[foo]`))
	testutil.ErrorMatches(t, err, `SerialFilter "\[foo\]" error: different filter`)
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)
}

func TestSerialFilter_ConditionalFilter(t *testing.T) {
	f := condfilter.SerialFilter(0x1234)
	testutil.Equal(t, f.ConditionalFilter(), `[0x1234]`)
}

func TestSerialFilter_MatchesSerial(t *testing.T) {
	f := condfilter.SerialFilter(0x1234)

	testutil.Equal(t, f.MatchesSerial(0x1234), true)
	testutil.Equal(t, f.MatchesSerial(0x5671), false)
}

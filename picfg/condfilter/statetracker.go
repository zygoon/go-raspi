// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.
// SPDX-FileCopyrightText: Zygmunt Krynicki

package condfilter

import (
	"fmt"
)

// StateTracker tracks effective conditional filters.
type StateTracker struct {
	// Model and board-type filters are mutually exclusive.
	// https://twitter.com/zygoon/status/1475261300922007557
	model *PiModelFilter
	board *BoardTypeFilter

	edid    *EDIDFilter
	gpio    *GPIOFilter
	serial  *SerialFilter
	hdmi    *HDMIFilter
	tryboot *TryBootFilter

	none *NoneFilter
}

// Filter updates the effective set of filters based on the given filter.
//
// Filters of a given type all override each other. Filters of distinct types
// stack. As a special case [all] resets all filters and [none] replaces any
// filters with itself.
//
// Filters can be passed by value or as a pointer.
func (tracker *StateTracker) Filter(filter ConditionalFilter) {
	switch f := filter.(type) {
	case AllFilter, *AllFilter:
		tracker.model = nil
		tracker.board = nil
		tracker.edid = nil
		tracker.gpio = nil
		tracker.serial = nil
		tracker.hdmi = nil
		tracker.tryboot = nil
		tracker.none = nil
	case PiModelFilter:
		tracker.model = &f
		tracker.board = nil
	case *PiModelFilter:
		tracker.model = f
		tracker.board = nil
	case EDIDFilter:
		tracker.edid = &f
	case *EDIDFilter:
		tracker.edid = f
	case GPIOFilter:
		tracker.gpio = &f
	case *GPIOFilter:
		tracker.gpio = f
	case SerialFilter:
		tracker.serial = &f
	case *SerialFilter:
		tracker.serial = f
	case HDMIFilter:
		tracker.hdmi = &f
	case *HDMIFilter:
		tracker.hdmi = f
	case TryBootFilter:
		tracker.tryboot = &f
	case *TryBootFilter:
		tracker.tryboot = f
	case BoardTypeFilter:
		tracker.model = nil
		tracker.board = &f
	case *BoardTypeFilter:
		tracker.model = nil
		tracker.board = f
	case NoneFilter:
		tracker.model = nil
		tracker.board = nil
		tracker.edid = nil
		tracker.gpio = nil
		tracker.serial = nil
		tracker.hdmi = nil
		tracker.tryboot = nil
		tracker.none = &f
	case *NoneFilter:
		tracker.model = nil
		tracker.board = nil
		tracker.edid = nil
		tracker.gpio = nil
		tracker.serial = nil
		tracker.hdmi = nil
		tracker.tryboot = nil
		tracker.none = f
	default:
		// This can only happen if new type is added not handled here
		panic(fmt.Sprintf("unexpected filter type: %T", filter))
	}
}

// IsAll returns true if current EffectiveFilters are equivalent to [all].
func (tracker *StateTracker) IsAll() bool {
	return tracker.model == nil && tracker.board == nil && tracker.edid == nil && tracker.gpio == nil && tracker.serial == nil && tracker.hdmi == nil && tracker.none == nil && tracker.tryboot == nil
}

// IsNone returns true if current EffectiveFilters are equivalent to [none].
func (tracker *StateTracker) IsNone() bool {
	return tracker.none != nil
}

// EffectiveFilters returns a slice with the set of effective conditional filters.
func (tracker *StateTracker) EffectiveFilters() []ConditionalFilter {
	fs := make([]ConditionalFilter, 0, 5) // 5 types may stack or [none]
	if tracker.model != nil {
		fs = append(fs, *tracker.model)
	} else if tracker.board != nil {
		fs = append(fs, *tracker.board)
	}

	if tracker.edid != nil {
		fs = append(fs, *tracker.edid)
	}

	if tracker.gpio != nil {
		fs = append(fs, *tracker.gpio)
	}

	if tracker.serial != nil {
		fs = append(fs, *tracker.serial)
	}

	if tracker.hdmi != nil {
		fs = append(fs, *tracker.hdmi)
	}

	if tracker.tryboot != nil {
		fs = append(fs, *tracker.tryboot)
	}

	if tracker.none != nil {
		fs = append(fs, *tracker.none)
	}

	if len(fs) == 0 {
		fs = append(fs, All)
	}

	return fs
}

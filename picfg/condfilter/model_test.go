// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter_test

import (
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/picfg/condfilter"
	"gitlab.com/zygoon/go-raspi/pimodel"
)

var allModelFilters = map[string]condfilter.PiModelFilter{
	"[pi0]":   condfilter.Pi0,
	"[pi0w]":  condfilter.Pi0W,
	"[pi02]":  condfilter.Pi02,
	"[pi1]":   condfilter.Pi1,
	"[pi2]":   condfilter.Pi2,
	"[pi3]":   condfilter.Pi3,
	"[pi3+]":  condfilter.Pi3Plus,
	"[pi4]":   condfilter.Pi4,
	"[pi400]": condfilter.Pi400,
	"[pi5]":   condfilter.Pi5,
	"[cm4]":   condfilter.Cm4,
	"[cm4s]":  condfilter.Cm4S,
}

func TestPiModelFilter_UnmarshalText(t *testing.T) {
	var f condfilter.PiModelFilter

	for text, filter := range allModelFilters {
		err := f.UnmarshalText([]byte(text))
		testutil.Ok(t, err)
		testutil.Equal(t, f, filter)
	}

	err := f.UnmarshalText([]byte(`[foo`))
	testutil.ErrorMatches(t, err, `PiModelFilter "\[foo" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`foo]`))
	testutil.ErrorMatches(t, err, `PiModelFilter "foo\]" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`[foo]`))
	testutil.ErrorMatches(t, err, `PiModelFilter "\[foo\]" error: different filter`)
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)
}

func TestPiModelFilter_MarshalText(t *testing.T) {
	for text, filter := range allModelFilters {
		b, err := filter.MarshalText()
		testutil.Ok(t, err)
		testutil.Equal(t, string(b), text)
	}

	f := condfilter.PiModelFilter(1000000)
	b, err := f.MarshalText()
	testutil.Ok(t, err)
	// XXX: Perhaps unexpected?
	testutil.Equal(t, string(b), "")
}

func TestPiModelFilter_MatchesModel(t *testing.T) {
	testMatrix := map[condfilter.PiModelFilter]map[pimodel.Model]bool{
		// [pi0] matches Pi0, Pi0W and Pi02W
		condfilter.Pi0: {
			pimodel.Pi0:   true,
			pimodel.Pi0W:  true,
			pimodel.Pi02W: true,
		},
		// [pi0w] matches both Pi0 and Pi0W
		condfilter.Pi0W: {
			pimodel.Pi0:  true,
			pimodel.Pi0W: true,
		},
		// [pi02] matches Pi0, Pi0W and Pi02W
		condfilter.Pi02: {
			pimodel.Pi0:   true,
			pimodel.Pi0W:  true,
			pimodel.Pi02W: true,
		},
		// [pi1] matches Pi1A, Pi1B and Cm1
		condfilter.Pi1: {
			pimodel.Pi1A: true,
			pimodel.Pi1B: true,
			pimodel.Cm1:  true,
		},
		// [pi2] matches Pi2B
		condfilter.Pi2: {
			pimodel.Pi2B: true,
		},
		// [pi3] matches Pi3B, Pi3APlus, Pi3BPlus and Cm3
		condfilter.Pi3: {
			pimodel.Pi3B:     true,
			pimodel.Pi3APlus: true,
			pimodel.Pi3BPlus: true,
			pimodel.Cm3:      true,
		},
		// [pi3+] matches Pi3APlus and Pi3BPlus
		condfilter.Pi3Plus: {
			pimodel.Pi3APlus: true,
			pimodel.Pi3BPlus: true,
		},
		// [pi4] matches Pi4B, Pi400 and Cm4
		condfilter.Pi4: {
			pimodel.Pi4B:  true,
			pimodel.Pi400: true,
			pimodel.Cm4:   true,
			pimodel.Cm4S:  true,
		},
		// [pi400] matches only Pi400
		condfilter.Pi400: {
			pimodel.Pi400: true,
		},
		// [pi5] matches only Pi5B
		condfilter.Pi5: {
			pimodel.Pi5: true,
		},
		// [cm4] matches only Cm4
		condfilter.Cm4: {
			pimodel.Cm4: true,
		},
		// [cm4s] matches only Cm4S
		condfilter.Cm4S: {
			pimodel.Cm4S: true,
		},
	}

	// Perform exhaustive check
	for filter, cases := range testMatrix {
		t.Run(filter.ConditionalFilter(), func(t *testing.T) {
			for model := pimodel.Model(0); model < pimodel.Pi5; model++ {
				shouldMatch := cases[model]
				testutil.Equal(t, filter.MatchesModel(model), shouldMatch)
			}
		})
	}
}

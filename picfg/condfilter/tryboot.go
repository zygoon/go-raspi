// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package condfilter

// TryBootFilter is a autoboot.txt conditional filter coupled to the in-memory "tryboot" state bit.
//
// Any configuration options placed in the [tryboot] section are only respected when the bit is on,
// immediately following a try-boot reboot request.
//
// See upstream documentation at
// https://www.raspberrypi.com/documentation/computers/config_txt.html#the-tryboot-filter
type TryBootFilter struct{}

// TryBoot is an instance of TryBootFilter.
var TryBoot = TryBootFilter{}

// MarshalText implements TextMarshaler.
func (tryboot TryBootFilter) MarshalText() ([]byte, error) {
	return []byte(tryboot.ConditionalFilter()), nil
}

// UnmarshalText implements TextUnmarshaler.
func (tryboot *TryBootFilter) UnmarshalText(text []byte) error {
	const typ = "TryBootFilter"

	name, err := parseName(text, typ)
	if err != nil {
		return err
	}

	if name != "tryboot" {
		return &FilterError{Type: typ, Filter: string(text), Err: ErrDifferent}
	}

	return nil
}

// ConditionalFilter implements ConfigFilter.
func (none TryBootFilter) ConditionalFilter() string {
	return "[tryboot]"
}

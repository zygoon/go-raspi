// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.
// SPDX-FileCopyrightText: Zygmunt Krynicki

package condfilter

import (
	"fmt"

	"gitlab.com/zygoon/go-raspi/picfg"
	"gitlab.com/zygoon/go-raspi/pimodel"
)

// Simulator simulates how the firmware interprets config.txt on a specific device.
//
// The simulator has limits and does not work with configuration files which use
// [gpioN=*], [EDID=*], [hdmi:N], [tryboot] conditional filters.
//
// Model-specific sections as well as conditional filter stacking are supported.
// This should provide a good-enough balance between supporting practical
// config.txt files correctly without guessing too much.
type Simulator struct {
	cfg     *picfg.ConfigTxt
	revCode pimodel.RevisionCode
	serial  pimodel.SerialNumber
	model   pimodel.Model
	env     SimulatorEnvironment
}

// NewSimulator returns a new Simulator sharing the given ConfigTxt.
//
// The simulator cannot be created if the revision code is invalid or if the
// given configuration uses conditional filters other than [none], [all],
// serial-number or any of the model-specific filters. If this is detected then
// NewSimulator fails with UnsupportedFilterError.
//
// The configuration object is bound to the simulator and is modified by the
// SetCoherentParameter function, and should not be modified elsewhere.
//
// The returned simulator does not support dynamically changing environment
// properties. Use NewDynamicSimulator to obtain this functionality.
func NewSimulator(cfg *picfg.ConfigTxt, revCode pimodel.RevisionCode, serial pimodel.SerialNumber) (*Simulator, error) {
	return NewDynamicSimulator(cfg, revCode, serial, nil)
}

// NewDynamicSimulator returns a new Simulator sharing the given ConfigTxt.
//
// This is the same as NewSimulator with the exception that the returned
// simulator supports [tryboot], [EDID=*], [hdmi:N] and [gpioN=*] filters.
func NewDynamicSimulator(cfg *picfg.ConfigTxt, revCode pimodel.RevisionCode, serial pimodel.SerialNumber, env SimulatorEnvironment) (*Simulator, error) {
	model, err := revCode.BoardType()
	if err != nil {
		return nil, err
	}

	// Check if all the conditional filters are recognized and supported.
	for sectIdx := range cfg.Sections {
		if filterText := cfg.Sections[sectIdx].Filter; filterText != "" {
			filter, err := Unmarshal([]byte(filterText))
			if err != nil {
				return nil, err
			}

			switch filter.(type) {
			case PiModelFilter:
				// We can resolve model filters.
			case BoardTypeFilter:
				// We can resolve board-type filters.
			case AllFilter:
				// [all] applies to all the boards.
			case NoneFilter:
				// [none] applies to none of the boards.
			case SerialFilter:
				// [0x1234] applies to the board with the same serial number.
			case TryBootFilter:
				// [tryboot] applies to boards that have the in-memory tryboot bit set.
				if env == nil {
					return nil, unsupportedFilter(filterText, cfg.Sections[sectIdx].Location)
				}
			case EDIDFilter:
				// [EDID=*] applies to boards that have the given EDID on any attached display.
				if env == nil {
					return nil, unsupportedFilter(filterText, cfg.Sections[sectIdx].Location)
				}
			case HDMIFilter:
				// [HDMI:N] applies to display-specific settings.
				if env == nil {
					return nil, unsupportedFilter(filterText, cfg.Sections[sectIdx].Location)
				}
			case GPIOFilter:
				// [gpioN=*] applies to boards that have the given GPIO pin in the matching state.
				if env == nil {
					return nil, unsupportedFilter(filterText, cfg.Sections[sectIdx].Location)
				}
			default:
				return nil, unsupportedFilter(filterText, cfg.Sections[sectIdx].Location)
			}
		}
	}

	sim := &Simulator{
		cfg:     cfg,
		revCode: revCode,
		serial:  serial,
		model:   model,
		env:     env,
	}

	return sim, nil
}

func unsupportedFilter(filterText string, loc picfg.Location) error {
	return &UnsupportedFilterError{Filter: filterText, Location: loc}
}

// SimulatorEnvironment encapsulates non-static properties relevant to firmware simulator.
type SimulatorEnvironment interface {
	// TryBootFlag returns true if the "tryboot" in-memory flag is enabled.
	// This is used by the [tryboot] filter.
	TryBootFlag() bool
	// IsGpioPinHigh returns true if the given GPIO pin is in the "high" state.
	// This is used by the [gpioN=*] filter.
	IsGpioPinHigh(pinNumber uint) bool
	// DisplayEdid returns the EDID string of an attached display.
	// The displayNumber argument is either 0 or 1.
	// This is used by the [EDID=*] filter.
	DisplayEdid(displayNumber uint) string
	// DisplayPortNumber returns the index of the display being introspected.
	// This is used by the [HDMI:N] filter.
	DisplayPortNumber() uint
}

// Parameter returns the last picfg.Parameter with the given name.
//
// The returned parameter is the last instance of a parameter with the given
// name, in a section that matches the hardware that BootConfig was instantiated
// for. If a parameter with the given name is not found, nil is returned without
// an error.
//
// On hardware other than what the BootConfig is instantiated for, or under
// exceptional circumstances, the firmware may load a different value for the
// parameter, for example when using a serial-number, GPIO or EDID conditional
// filter. If more consistency and predictability is required, use
// CoherentParameter instead.
func (sim *Simulator) Parameter(name string) (lastParam *picfg.Parameter) {
	var tracker StateTracker

	for sectIdx := range sim.cfg.Sections {
		if filterText := sim.cfg.Sections[sectIdx].Filter; filterText != "" {
			// Unmarshal the filter and feed it to the state tracker.
			filter := MustUnmarshal([]byte(filterText))
			tracker.Filter(filter)
		}

		// Skip sections that do not match the filter predicate.
		if !sim.filterPredicate(tracker.EffectiveFilters()) {
			continue
		}

		// Find the last definition of a parameter with the name we are looking
		// for. Empirical testing shows that when a parameter is placed in
		// multiple matching sections and when there are multiple definitions of
		// said parameter, the last definition is used.
		for paramIdx := range sim.cfg.Sections[sectIdx].Parameters {
			if param := &sim.cfg.Sections[sectIdx].Parameters[paramIdx]; param.Name == name {
				lastParam = param
			}
		}
	}

	return lastParam
}

// CoherentParameter returns the last Parameter with the given name.
//
// The returned parameter is the last instance of a parameter with the given
// name, in a section that matches the hardware that BootConfig was instantiated
// for. If a parameter with the given name is not found, nil is returned without
// an error.
//
// The returned parameter should not be modified. To modify the value in a
// predictable way use SetCoherentParameter.
//
// Unlike Parameter, CoherentParameter ensures that all the definitions of the
// parameter, in most the conditional sections, even those that do not match the
// model for which BootConfig was instantiated for but except for [none], which
// is never inspected by the simulator, have the exact same value. If in any
// circumstances it is possible to observe a different value,
// CoherentParameter fails with NonCoherentParameterError.
func (sim *Simulator) CoherentParameter(name string) (*picfg.Parameter, error) {
	var tracker StateTracker

	var firstParam *picfg.Parameter

	var lastMatchingParam *picfg.Parameter

	for sectIdx := range sim.cfg.Sections {
		if filterText := sim.cfg.Sections[sectIdx].Filter; filterText != "" {
			// Unmarshal the filter and feed it to the state tracker.
			filter := MustUnmarshal([]byte(filterText))
			tracker.Filter(filter)
		}

		// Skip [none] as a special case.
		if tracker.IsNone() {
			continue
		}

		isMatching := sim.filterPredicate(tracker.EffectiveFilters())

		for paramIdx := range sim.cfg.Sections[sectIdx].Parameters {
			param := &sim.cfg.Sections[sectIdx].Parameters[paramIdx]

			if param.Name != name {
				continue
			}

			if isMatching {
				lastMatchingParam = param
			}

			if firstParam == nil {
				firstParam = param
				continue
			}

			if firstParam.Value != param.Value {
				return nil, &NonCoherentParameterError{
					firstParam:    *firstParam,
					clashingParam: *param,
				}
			}
		}
	}

	return lastMatchingParam, nil
}

// SetCoherentParameter coherently sets the value of a parameter.
//
// The existing parameter with a given name is modified in all the conditional
// sections, except for [none], which is never inspected by the simulator. If
// the parameter is non-coherent, no modification is done and the function fails
// with NonCoherentParameterError.
//
// If the parameter cannot be found it is added to the last section which is
// equivalent to [all]. If no such section exists it is appended and used to
// store the parameter.
//
// For a distinction of between coherent and non-coherent parameters consult the
// documentation of CoherentParameter.
func (sim *Simulator) SetCoherentParameter(name, value string) error {
	var tracker StateTracker

	var params []*picfg.Parameter

	var goodSect *picfg.Section

	for sectIdx := range sim.cfg.Sections {
		if filterText := sim.cfg.Sections[sectIdx].Filter; filterText != "" {
			// Unmarshal the filter and feed it to the state tracker.
			filter := MustUnmarshal([]byte(filterText))
			tracker.Filter(filter)
		}

		// Skip [none] as a special case.
		if tracker.IsNone() {
			continue
		}

		// If this is the [all] section or equivalent, remember it as a
		// candidate to place the parameter in case we have to add it.
		if tracker.IsAll() {
			goodSect = &sim.cfg.Sections[sectIdx]
		}

		// Note that we are not checking if the section conditional filter
		// matches our filter predicate (which effectively checks for model),
		// because we want to update all the occurrences of the parameter.
		for paramIdx := range sim.cfg.Sections[sectIdx].Parameters {
			if param := &sim.cfg.Sections[sectIdx].Parameters[paramIdx]; param.Name == name {
				// Remember all the matching parameters.
				params = append(params, param)
			}
		}
	}

	// If we have existing parameters we can try to update them.
	if len(params) > 0 {
		// Are the parameters coherent?
		for _, param := range params[1:] {
			if param.Value != params[0].Value {
				return &NonCoherentParameterError{
					firstParam:    *params[0],
					clashingParam: *param,
				}
			}
		}
		// Update all the parameters.
		for _, param := range params {
			param.Value = value
		}

		return nil
	}

	// Do we have a section to put a new coherent value into? This can be the
	// last [all] section or the first section if it comes without a filter.
	if goodSect == nil {
		sim.cfg.Sections = append(sim.cfg.Sections, picfg.Section{Filter: "[all]"})
		goodSect = &sim.cfg.Sections[len(sim.cfg.Sections)-1]
	}

	goodSect.Parameters = append(goodSect.Parameters, picfg.Parameter{
		Name:  name,
		Value: value,
	})

	return nil
}

// filterPredicate returns true if the given set of effective filters matches
// should be interpreted as matching the simulated device.
//
// The filter [all] always matches. The filter [none] never matches. The set of
// model-specific filters match only the model described by the revision code.
// All other filters are rejected in NewSimulator.
func (sim *Simulator) filterPredicate(filters []ConditionalFilter) bool {
	for _, filter := range filters {
		switch f := filter.(type) {
		case PiModelFilter:
			return f.MatchesModel(sim.model)
		case BoardTypeFilter:
			return f.MatchesRevCode(sim.revCode)
		case SerialFilter:
			return f.MatchesSerial(sim.serial)
		case AllFilter:
			return true
		case NoneFilter:
			return false
		case TryBootFilter:
			return sim.mustEnv().TryBootFlag()
		case GPIOFilter:
			return sim.mustEnv().IsGpioPinHigh(f.PinNumber) == f.High
		case HDMIFilter:
			// HDMI filters are "legacy" and are only supported on Raspiberry Pi 4:
			// https://www.raspberrypi.com/documentation/computers/legacy_config_txt.html#the-hdmi-filter
			if sim.model != pimodel.Pi4B && sim.model != pimodel.Pi400 {
				return false
			}

			return sim.mustEnv().DisplayPortNumber() == f.PortNumber

		case EDIDFilter:
			// EDID filters are not supported on Raspberry Pi 5:
			// https://www.raspberrypi.com/documentation/computers/config_txt.html#the-edid-filter
			if sim.model == pimodel.Pi5 {
				return false
			}

			return sim.mustEnv().DisplayEdid(0) == f.MonitorEDID() || sim.mustEnv().DisplayEdid(1) == f.MonitorEDID()
		default:
			// This can only happen if NewSimulator and filterPredicate become
			// out of sync.
			break
		}
	}
	// StateTracker.EffectiveFilters never returns an empty set, so we should
	// have matched something above. If we did not, then NewSimulator and
	// filterPredicate are out of sync. and we had to break in the default case
	// above.
	panic(fmt.Sprintf("NewSimulator is out of sync with filterPredicate, conditional filters: %#v", filters))
}

func (sim *Simulator) mustEnv() SimulatorEnvironment {
	if sim.env == nil {
		panic("Simulator made with NewSimulator attempted to access SimulatorEnvironment")
	}

	return sim.env
}

// NonCoherentParameterError records a parameter whose value differs across
// conditional sections.
type NonCoherentParameterError struct {
	firstParam    picfg.Parameter
	clashingParam picfg.Parameter
}

// Error implements the error interface.
func (e *NonCoherentParameterError) Error() string {
	return fmt.Sprintf("conflicting definition of %s,"+
		" first defined in %s as %s, then in %s as %s",
		e.firstParam.Name,
		&e.firstParam.Location, e.firstParam.Value,
		&e.clashingParam.Location, e.clashingParam.Value)
}

// UnsupportedFilterError records an unsupported conditional filter.
type UnsupportedFilterError struct {
	Filter   string
	Location picfg.Location
}

// Error implements the error interface.
func (e *UnsupportedFilterError) Error() string {
	return fmt.Sprintf("conditional filter %s (from %s) is not supported", e.Filter, &e.Location)
}

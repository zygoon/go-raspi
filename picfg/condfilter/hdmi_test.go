// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter_test

import (
	"strconv"
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/picfg/condfilter"
)

func TestHDMIFilter_UnmarshalText(t *testing.T) {
	var f condfilter.HDMIFilter

	err := f.UnmarshalText([]byte(`[hdmi:1]`))
	testutil.Ok(t, err)
	testutil.Equal(t, f.PortNumber, uint(1))

	err = f.UnmarshalText([]byte(`[hdmi:-1]`))
	testutil.ErrorMatches(t, err, `HDMIFilter "\[hdmi:-1\]" error: invalid HDMI port number: invalid syntax`)
	testutil.ErrorIs(t, err, strconv.ErrSyntax)
	paramErr := testutil.ErrorAs[*condfilter.ParameterError](t, err)
	testutil.Equal(t, paramErr.What, condfilter.HDMIPortNumber)

	err = f.UnmarshalText([]byte(`[hdmi:foo]`))
	testutil.ErrorMatches(t, err, `HDMIFilter "\[hdmi:foo\]" error: invalid HDMI port number: invalid syntax`)
	testutil.ErrorIs(t, err, strconv.ErrSyntax)
	paramErr = testutil.ErrorAs[*condfilter.ParameterError](t, err)
	testutil.Equal(t, paramErr.What, condfilter.HDMIPortNumber)

	err = f.UnmarshalText([]byte(`[hdmi:]`))
	testutil.ErrorMatches(t, err, `HDMIFilter "\[hdmi:\]" error: invalid HDMI port number: parameter value required`)
	testutil.ErrorIs(t, err, condfilter.ErrParameterRequired)
	paramErr = testutil.ErrorAs[*condfilter.ParameterError](t, err)
	testutil.Equal(t, paramErr.What, condfilter.HDMIPortNumber)

	err = f.UnmarshalText([]byte(`[hdmi]`))
	testutil.ErrorMatches(t, err, `HDMIFilter "\[hdmi\]" error: different filter`)
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)

	err = f.UnmarshalText([]byte(`[foo`))
	testutil.ErrorMatches(t, err, `HDMIFilter "\[foo" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`foo]`))
	testutil.ErrorMatches(t, err, `HDMIFilter "foo\]" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`[foo]`))
	testutil.ErrorMatches(t, err, `HDMIFilter "\[foo\]" error: different filter`)
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)

	err = f.UnmarshalText([]byte(`[foo=]`))
	testutil.ErrorMatches(t, err, `HDMIFilter "\[foo=\]" error: different filter`)
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)
}

func TestHDMIFilter_MarshalText(t *testing.T) {
	f := condfilter.HDMIFilter{PortNumber: 123}
	b, err := f.MarshalText()
	testutil.Ok(t, err)
	testutil.Equal(t, string(b), `[hdmi:123]`)
}

func TestHDMIFilter_ConditionalFilter(t *testing.T) {
	f := condfilter.HDMIFilter{PortNumber: 123}
	testutil.Equal(t, f.ConditionalFilter(), `[hdmi:123]`)
}

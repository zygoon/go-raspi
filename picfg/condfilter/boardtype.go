// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter

import (
	"bytes"
	"fmt"
	"strconv"

	"gitlab.com/zygoon/go-raspi/pimodel"
)

// BoardTypeFilter restricts subsequent boot options to specific boards.
// Unlike PiModelFilter, the match is precise and uses the board type
// identifier encoded in the revision code.
type BoardTypeFilter uint8

// MarshalText implements TextMarshaler.
func (f BoardTypeFilter) MarshalText() ([]byte, error) {
	return []byte(f.ConditionalFilter()), nil
}

// UnmarshalText implements TextUnmarshaler.
func (f *BoardTypeFilter) UnmarshalText(text []byte) error {
	const typ = "BoardTypeFilter"

	name, param, err := parseNameParam(text, typ, '=')
	if err != nil {
		return err
	}

	if name != "board-type" {
		return &FilterError{Type: typ, Filter: string(text), Err: ErrDifferent}
	}

	if param == "" {
		if !bytes.ContainsRune(text, '=') {
			// [board-type] is an altogether different filter.
			return &FilterError{Type: typ, Filter: string(text), Err: ErrDifferent}
		}
		// [board-type=] is an invalid use of the filter.
		return &FilterError{Type: typ, Filter: string(text), Err: &ParameterError{What: BoardTypeNumber, Err: ErrParameterRequired}}
	}

	// The board type can be either hex or decimal. Both have been tested experimentally.
	n, err := strconv.ParseUint(param, 0, 8)
	if err != nil {
		return &FilterError{Type: typ, Filter: string(text), Err: &ParameterError{
			What: BoardTypeNumber,
			Err:  err,
		}}
	}

	*f = BoardTypeFilter(n)

	return nil
}

// ConditionalFilter implements ConditionalFilter.
func (f BoardTypeFilter) ConditionalFilter() string {
	return fmt.Sprintf("[board-type=%#x]", uint8(f))
}

// MatchesRevCode returns true when the filter matches the given revision code.
func (f BoardTypeFilter) MatchesRevCode(r pimodel.RevisionCode) bool {
	if code, err := r.BoardTypeCode(); err == nil {
		return uint8(f) == code
	}

	return false
}

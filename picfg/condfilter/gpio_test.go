// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter_test

import (
	"strconv"
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/picfg/condfilter"
)

func TestGPIOFilter_UnmarshalText(t *testing.T) {
	var f condfilter.GPIOFilter

	err := f.UnmarshalText([]byte(`[gpio123=0]`))
	testutil.Ok(t, err)
	testutil.Equal(t, f, condfilter.GPIOFilter{PinNumber: 123, High: false})

	err = f.UnmarshalText([]byte(`[gpio123=1]`))
	testutil.Ok(t, err)
	testutil.Equal(t, f, condfilter.GPIOFilter{PinNumber: 123, High: true})

	err = f.UnmarshalText([]byte(`[gpio` + `foo=1]`))
	testutil.ErrorMatches(t, err, `GPIOFilter "\[gpio`+`foo=1\]" error: invalid GPIO pin number: invalid syntax`)
	testutil.ErrorIs(t, err, strconv.ErrSyntax)
	paramErr := testutil.ErrorAs[*condfilter.ParameterError](t, err)
	testutil.Equal(t, paramErr.What, condfilter.GPIOPinNumber)

	err = f.UnmarshalText([]byte(`[gpio=1]`))
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)

	err = f.UnmarshalText([]byte(`[gpio123=2]`))
	testutil.ErrorMatches(t, err, `GPIOFilter "\[gpio123=2\]" error: invalid GPIO pin state: value out of range`)
	testutil.ErrorIs(t, err, strconv.ErrRange)

	err = f.UnmarshalText([]byte(`[gpio123=bar]`))
	testutil.ErrorMatches(t, err, `GPIOFilter "\[gpio123=bar\]" error: invalid GPIO pin state: invalid syntax`)
	testutil.ErrorIs(t, err, strconv.ErrSyntax)
	paramErr = testutil.ErrorAs[*condfilter.ParameterError](t, err)
	testutil.Equal(t, paramErr.What, condfilter.GPIOPinState)

	err = f.UnmarshalText([]byte(`[gpio123=]`))
	testutil.ErrorMatches(t, err, `GPIOFilter "\[gpio123=\]" error: invalid GPIO pin state: parameter value required`)
	testutil.ErrorIs(t, err, condfilter.ErrParameterRequired)
	paramErr = testutil.ErrorAs[*condfilter.ParameterError](t, err)
	testutil.Equal(t, paramErr.What, condfilter.GPIOPinState)

	err = f.UnmarshalText([]byte(`[gpio123]`))
	testutil.ErrorMatches(t, err, `GPIOFilter "\[gpio123\]" error: different filter`)
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)

	err = f.UnmarshalText([]byte(`[foo`))
	testutil.ErrorMatches(t, err, `GPIOFilter "\[foo" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`foo]`))
	testutil.ErrorMatches(t, err, `GPIOFilter "foo\]" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`[foo]`))
	testutil.ErrorMatches(t, err, `GPIOFilter "\[foo\]" error: different filter`)
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)
}

func TestGPIOFilter_MarshalText(t *testing.T) {
	f := condfilter.GPIOFilter{PinNumber: 123, High: false}
	b, err := f.MarshalText()
	testutil.Ok(t, err)
	testutil.Equal(t, string(b), `[gpio123=0]`)

	f = condfilter.GPIOFilter{PinNumber: 123, High: true}
	b, err = f.MarshalText()
	testutil.Ok(t, err)
	testutil.Equal(t, string(b), `[gpio123=1]`)
}

func TestGPIOFilter_ConditionalFilter(t *testing.T) {
	f := condfilter.GPIOFilter{PinNumber: 123, High: false}
	testutil.Equal(t, f.ConditionalFilter(), `[gpio123=0]`)

	f = condfilter.GPIOFilter{PinNumber: 123, High: true}
	testutil.Equal(t, f.ConditionalFilter(), `[gpio123=1]`)
}

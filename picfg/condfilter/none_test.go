// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter_test

import (
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/picfg/condfilter"
)

func TestNoneFilter_MarshalText(t *testing.T) {
	text, err := condfilter.None.MarshalText()
	testutil.Ok(t, err)
	testutil.Equal(t, string(text), "[none]")
}

func TestNoneFilter_UnmarshalText(t *testing.T) {
	var f condfilter.NoneFilter
	err := f.UnmarshalText([]byte(`[none]`))
	testutil.Ok(t, err)

	err = f.UnmarshalText([]byte(`[foo`))
	testutil.ErrorMatches(t, err, `NoneFilter "\[foo" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`foo]`))
	testutil.ErrorMatches(t, err, `NoneFilter "foo\]" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`[foo]`))
	testutil.ErrorMatches(t, err, `NoneFilter "\[foo\]" error: different filter`)
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)
}

func TestNoneFilter_ConditionalFilter(t *testing.T) {
	testutil.Equal(t, condfilter.None.ConditionalFilter(), `[none]`)
}

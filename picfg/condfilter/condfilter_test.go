// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter_test

import (
	"errors"
	"strconv"
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/picfg/condfilter"
)

func TestConditionalFilter_Unmarshal(t *testing.T) {
	type testCase struct {
		text   string
		filter condfilter.ConditionalFilter
	}

	for i, tc := range []testCase{
		{"[all]", condfilter.All},
		{"[none]", condfilter.None},
		{"[pi0]", condfilter.Pi0},
		{"[pi0w]", condfilter.Pi0W},
		{"[pi1]", condfilter.Pi1},
		{"[pi2]", condfilter.Pi2},
		{"[pi3]", condfilter.Pi3},
		{"[pi3+]", condfilter.Pi3Plus},
		{"[pi4]", condfilter.Pi4},
		{"[pi400]", condfilter.Pi400},
		{"[cm4]", condfilter.Cm4},
		{"[EDID=foo]", condfilter.EDIDFilter("foo")},
		{"[gpio2=1]", condfilter.GPIOFilter{PinNumber: 2, High: true}},
		{"[hdmi:1]", condfilter.HDMIFilter{PortNumber: 1}},
		{"[0x1234]", condfilter.SerialFilter(0x1234)},
		{"[board-type=0x12]", condfilter.BoardTypeFilter(0x12)},
		{"[tryboot]", condfilter.TryBoot},
	} {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			f, err := condfilter.Unmarshal([]byte(tc.text))
			testutil.Ok(t, err)
			testutil.Equal(t, f, tc.filter)
		})
	}
}

func TestConditionalFilter_MustUnmarshal(t *testing.T) {
	testutil.Equal[condfilter.ConditionalFilter](t, condfilter.MustUnmarshal([]byte(`[all]`)), condfilter.All)
	testutil.PanicMatches(t, func() { _ = condfilter.MustUnmarshal([]byte(`[potato]`)) }, `conditional filter error "\[potato\]": unknown filter`)
}

func TestConditionalFilter_UnmarshalUnknownFilter(t *testing.T) {
	_, err := condfilter.Unmarshal([]byte("[potato]"))
	testutil.ErrorMatches(t, err, `conditional filter error "\[potato\]": unknown filter`)
}

func TestConditionalFilter_UnmarshalSyntaxError(t *testing.T) {
	_, err := condfilter.Unmarshal([]byte("[potato]"))
	testutil.ErrorMatches(t, err, `conditional filter error "\[potato\]": unknown filter`)

	for _, potato := range []string{"[potato", "potato]", "potato"} {
		_, err = condfilter.Unmarshal([]byte(potato))
		testutil.ErrorMatches(t, err, `conditional filter error ".*": syntax error`)
	}
}

var errJustTesting = errors.New("just testing")

func TestFilterError_Error(t *testing.T) {
	// Typeless form
	err := &condfilter.FilterError{
		Filter: "[foo]",
		Err:    errJustTesting,
	}
	testutil.ErrorMatches(t, err, `conditional filter error "\[foo\]": just testing`)

	// Type-specific form
	err.Type = "FooType"
	testutil.ErrorMatches(t, err, `FooType "\[foo\]" error: just testing`)
}

func TestFilterError_Unwrap(t *testing.T) {
	err := &condfilter.FilterError{Err: errJustTesting}
	testutil.ErrorMatches(t, errors.Unwrap(err), `just testing`)
}

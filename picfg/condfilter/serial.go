// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/zygoon/go-raspi/pimodel"
)

// SerialFilter restricts subsequent configuration options to a specific serial number of the SoC.
type SerialFilter pimodel.SerialNumber

// MarshalText implements TextMarshaler.
func (serial SerialFilter) MarshalText() ([]byte, error) {
	return []byte(serial.ConditionalFilter()), nil
}

// UnmarshalText implements TextUnmarshaler.
func (serial *SerialFilter) UnmarshalText(text []byte) error {
	const typ = "SerialFilter"

	name, err := parseName(text, typ)
	if err != nil {
		return err
	}

	if !strings.HasPrefix(name, "0x") {
		return &FilterError{Type: typ, Filter: string(text), Err: ErrDifferent}
	}

	n, err := strconv.ParseUint(string(text[3:len(text)-1]), 16, 64)
	if err != nil {
		return &FilterError{Type: typ, Filter: string(text), Err: &ParameterError{
			What: CPUSerialNumber,
			Err:  err,
		}}
	}

	*serial = SerialFilter(n)

	return nil
}

// ConditionalFilter implements ConditionalFilter.
func (serial SerialFilter) ConditionalFilter() string {
	return fmt.Sprintf("[%#x]", uint64(serial))
}

// MatchesSerial returns true if the given serial number is the same.
func (serial SerialFilter) MatchesSerial(s pimodel.SerialNumber) bool {
	return pimodel.SerialNumber(serial) == s
}

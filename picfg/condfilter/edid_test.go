// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter_test

import (
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/picfg/condfilter"
)

func TestEDIDFilter_MarshalText(t *testing.T) {
	text, err := condfilter.EDIDFilter("foo").MarshalText()
	testutil.Ok(t, err)
	testutil.Equal(t, string(text), "[EDID=foo]")
}

func TestEDIDFilter_UnmarshalText(t *testing.T) {
	var f condfilter.EDIDFilter

	err := f.UnmarshalText([]byte(`[EDID=value]`))
	testutil.Ok(t, err)
	testutil.Equal(t, string(f), "value")

	err = f.UnmarshalText([]byte(`[EDID=]`))
	testutil.ErrorMatches(t, err, `EDIDFilter "\[EDID=\]" error: invalid monitor EDID: parameter value required`)
	testutil.ErrorIs(t, err, condfilter.ErrParameterRequired)
	paramErr := testutil.ErrorAs[*condfilter.ParameterError](t, err)
	testutil.Equal(t, paramErr.What, condfilter.MonitorEDID)

	err = f.UnmarshalText([]byte(`[EDID]`))
	testutil.ErrorMatches(t, err, `EDIDFilter "\[EDID\]" error: different filter`)
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)

	err = f.UnmarshalText([]byte(`[foo`))
	testutil.ErrorMatches(t, err, `EDIDFilter "\[foo" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`foo]`))
	testutil.ErrorMatches(t, err, `EDIDFilter "foo\]" error: syntax error`)
	testutil.ErrorIs(t, err, condfilter.ErrSyntax)

	err = f.UnmarshalText([]byte(`[foo]`))
	testutil.ErrorMatches(t, err, `EDIDFilter "\[foo\]" error: different filter`)
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)

	err = f.UnmarshalText([]byte(`[foo=]`))
	testutil.ErrorMatches(t, err, `EDIDFilter "\[foo=\]" error: different filter`)
	testutil.ErrorIs(t, err, condfilter.ErrDifferent)
}

func TestEDIDFilter_ConditionalFilter(t *testing.T) {
	f := condfilter.EDIDFilter("foo")
	testutil.Equal(t, f.ConditionalFilter(), `[EDID=foo]`)
}

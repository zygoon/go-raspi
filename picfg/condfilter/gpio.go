// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
)

// GPIOFilter restricts subsequent configuration options to the runtime state of given GPIO pins.
type GPIOFilter struct {
	PinNumber uint
	High      bool
}

// MarshalText implements TextMarshaler.
func (gpio GPIOFilter) MarshalText() ([]byte, error) {
	return []byte(gpio.ConditionalFilter()), nil
}

// UnmarshalText implements TextUnmarshaler.
func (gpio *GPIOFilter) UnmarshalText(text []byte) (err error) {
	const typ = "GPIOFilter"

	const namePrefix = "gpio"

	name, pinStateText, err := parseNameParam(text, typ, '=')
	if err != nil {
		return err
	}

	if !strings.HasPrefix(name, namePrefix) {
		return &FilterError{Type: typ, Filter: string(text), Err: ErrDifferent}
	}

	pinNumberText := strings.TrimPrefix(name, namePrefix)
	if pinNumberText == "" {
		// Treat [gpio] as an altogether different filter.
		return &FilterError{Type: typ, Filter: string(text), Err: ErrDifferent}
	}

	if pinStateText == "" {
		if !bytes.ContainsRune(text, '=') {
			// [gpioN] is an altogether different filter.
			return &FilterError{Type: typ, Filter: string(text), Err: ErrDifferent}
		}
		// [gpioN=] is an invalid use of the [gpioN:M] filter.
		return &FilterError{Type: typ, Filter: string(text), Err: &ParameterError{What: GPIOPinState, Err: ErrParameterRequired}}
	}

	pinNumber, err := strconv.ParseUint(pinNumberText, 10, 16)
	if err != nil {
		return &FilterError{Type: typ, Filter: string(text), Err: &ParameterError{
			What: GPIOPinNumber,
			Err:  err,
		}}
	}

	high, err := strconv.ParseUint(pinStateText, 10, 16)
	if err == nil && high > 1 {
		err = strconv.ErrRange
	}

	if err != nil {
		return &FilterError{Type: typ, Filter: string(text), Err: &ParameterError{
			What: GPIOPinState,
			Err:  err,
		}}
	}

	gpio.PinNumber = uint(pinNumber)
	gpio.High = high == 1

	return nil
}

// ConditionalFilter implements ConditionalFilter.
func (gpio GPIOFilter) ConditionalFilter() string {
	high := 0
	if gpio.High {
		high = 1
	}

	return fmt.Sprintf("[gpio%d=%d]", gpio.PinNumber, high)
}

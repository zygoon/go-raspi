// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package condfilter

import (
	"gitlab.com/zygoon/go-raspi/pimodel"
)

// PiModelFilter restricts subsequent boot options to a subset of Raspberry Pi models.
//
// Model filters are documented at
// https://www.raspberrypi.com/documentation/computers/config_txt.html#model-filters
type PiModelFilter int

const (
	// Pi0 represents the Raspberry Pi Zero, Zero W or Zero WH.
	Pi0 PiModelFilter = iota
	// Pi0W represents the Raspberry Pi Zero W or Zero WH.
	Pi0W
	// Pi02 represents the Raspberry Pi Zero 2.
	Pi02
	// Pi1 represents the Raspberry Pi 1 model A, model B or the Compute-Module.
	Pi1
	// Pi2 represents the Raspberry Pi model 2B.
	Pi2
	// Pi3 represents the Raspberry Pi models 3B or 3B+.
	Pi3
	// Pi3Plus represents the Raspberry Pi model 3B+.
	Pi3Plus
	// Pi4 represents the Raspberry Pi model 4B, Pi400 or Cm4.
	Pi4
	// Pi400 represents the Raspberry Pi 400 - all-in-one computer.
	Pi400
	// Cm4 represents the Raspberry Pi Compute Module 4.
	Cm4
	// Cm4s represents the Raspberry Pi Compute Module 4 ("S" variant).
	Cm4S
	// Pi5 represents the Raspberrty Pi model 5B.
	Pi5
)

// MatchesModel returns true if the given model filter matches a given model.
//
// The way a particular conditional filter matches to a Raspberry Pi model is documented at
// https://www.raspberrypi.org/documentation/configuration/config-txt/conditional.md
func (modelFilter PiModelFilter) MatchesModel(model pimodel.Model) bool {
	switch modelFilter {
	case Pi1:
		return model == pimodel.Pi1A || model == pimodel.Pi1B || model == pimodel.Cm1
	case Pi2:
		return model == pimodel.Pi2B
	case Pi3:
		return model == pimodel.Pi3B || model == pimodel.Pi3BPlus || model == pimodel.Pi3APlus || model == pimodel.Cm3
	case Pi3Plus:
		return model == pimodel.Pi3APlus || model == pimodel.Pi3BPlus
	case Pi4:
		return model == pimodel.Pi4B || model == pimodel.Pi400 || model == pimodel.Cm4 || model == pimodel.Cm4S
	case Pi400:
		return model == pimodel.Pi400
	case Pi5:
		return model == pimodel.Pi5
	case Cm4:
		return model == pimodel.Cm4
	case Cm4S:
		return model == pimodel.Cm4S
	case Pi0:
		return model == pimodel.Pi0 || model == pimodel.Pi0W || model == pimodel.Pi02W
	case Pi0W:
		return model == pimodel.Pi0W || model == pimodel.Pi0
	case Pi02:
		return model == pimodel.Pi02W || model == pimodel.Pi0W || model == pimodel.Pi0
	default:
		return false
	}
}

const (
	pi0Section     = "[pi0]"
	pi0wSection    = "[pi0w]"
	pi02Section    = "[pi02]"
	pi1Section     = "[pi1]"
	pi2Section     = "[pi2]"
	pi3Section     = "[pi3]"
	pi3PlusSection = "[pi3+]"
	pi4Section     = "[pi4]"
	pi400Section   = "[pi400]"
	cm4Section     = "[cm4]"
	cm4SSection    = "[cm4s]"
	pi5Section     = "[pi5]"
)

// UnmarshalText implements TextUnmarshaler.
func (modelFilter *PiModelFilter) UnmarshalText(text []byte) error {
	const typ = "PiModelFilter"

	_, err := parseName(text, typ)
	if err != nil {
		return err
	}

	switch s := string(text); s {
	case pi0Section:
		*modelFilter = Pi0
	case pi0wSection:
		*modelFilter = Pi0W
	case pi02Section:
		*modelFilter = Pi02
	case pi1Section:
		*modelFilter = Pi1
	case pi2Section:
		*modelFilter = Pi2
	case pi3Section:
		*modelFilter = Pi3
	case pi3PlusSection:
		*modelFilter = Pi3Plus
	case pi4Section:
		*modelFilter = Pi4
	case pi400Section:
		*modelFilter = Pi400
	case pi5Section:
		*modelFilter = Pi5
	case cm4Section:
		*modelFilter = Cm4
	case cm4SSection:
		*modelFilter = Cm4S
	default:
		return &FilterError{Type: typ, Filter: string(text), Err: ErrDifferent}
	}

	return nil
}

// MarshalText implements TextMarshaler.
func (modelFilter PiModelFilter) MarshalText() ([]byte, error) {
	return []byte(modelFilter.ConditionalFilter()), nil
}

// ConditionalFilter implements ConditionalFilter.
func (modelFilter PiModelFilter) ConditionalFilter() string {
	switch modelFilter {
	case Pi0:
		return pi0Section
	case Pi0W:
		return pi0wSection
	case Pi02:
		return pi02Section
	case Pi1:
		return pi1Section
	case Pi2:
		return pi2Section
	case Pi3:
		return pi3Section
	case Pi3Plus:
		return pi3PlusSection
	case Pi4:
		return pi4Section
	case Pi400:
		return pi400Section
	case Pi5:
		return pi5Section
	case Cm4:
		return cm4Section
	case Cm4S:
		return cm4SSection
	default:
		return ""
	}
}

// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package picfg

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
)

var errTooLong = errors.New("cannot parse line longer than 98 bytes")

// Decoder reads and decodes config.txt from an input stream.
type Decoder struct {
	r                io.Reader
	preserveComments bool
}

// NewDecoder returns a decoder that reads from r.
func NewDecoder(r io.Reader) *Decoder {
	return &Decoder{r: r}
}

// PreserveComments makes the decoder preserve comments.
//
// Comments are associated with files, sections, parameters.
//
// IMPORTANT: Precise association of comments may change over time. All comments
// are parsed and preserved but the exact logic if a comment is associated to
// the preceding or following entity is not covered by API stability guarantees.
func (dec *Decoder) PreserveComments() {
	dec.preserveComments = true
}

func (dec *Decoder) fileName() string {
	if f, ok := dec.r.(*os.File); ok {
		return f.Name()
	}

	return ""
}

// Decode reads the complete config.txt file and stores it in cfg.
func (dec *Decoder) Decode(cfg *ConfigTxt) error {
	var newCfg ConfigTxt

	var seqNum uint

	fileName := dec.fileName()
	sect := Section{Location: Location{FileName: fileName}}

	var comments []string

	scanner := bufio.NewScanner(dec.r)
	for lineno := 1; scanner.Scan(); lineno++ {
		line := scanner.Bytes()
		// Ignore comments if they span the whole line.
		//
		// Note that this cheats, and avoids checking the 98-character limit for
		// comments. Casual observation seems to indicate that this is
		// consistent with firmware behavior.
		if bytes.IndexRune(line, '#') == 0 {
			if dec.preserveComments {
				comments = append(comments, scanner.Text())
			}

			continue
		}
		// Choke on lines longer than 98 characters.
		// https://www.raspberrypi.com/documentation/computers/config_txt.html#file-format
		if len(line) > 98 {
			return &ParseError{LineNo: lineno, Err: errTooLong}
		}

		// Strip _trailing_ spaces.
		line = bytes.TrimRight(line, "\n\r\t ")

		// Skip empty lines.
		if len(line) == 0 {
			if dec.preserveComments {
				comments = append(comments, scanner.Text())
			}

			continue
		}

		if bytes.HasPrefix(line, []byte(`[`)) && bytes.HasSuffix(line, []byte(`]`)) {
			if sect.Location.LineNo == 0 {
				seqNum++

				sect.Location.LineNo = lineno
				sect.Location.SeqNum = seqNum
			}

			// The section we are working with already has a filter. Append it
			// to the section list and prepare for looking at the next section.
			if sect.Filter != "" {
				newCfg.Sections = append(newCfg.Sections, sect)

				seqNum++

				sect = Section{Location: Location{FileName: fileName, LineNo: lineno, SeqNum: seqNum}}
			}

			sect.Filter = string(line)

			if dec.preserveComments {
				sect.Comments = comments
				comments = nil
			}
		} else {
			var param Parameter
			if err := param.UnmarshalText(line); err != nil {
				return &ParseError{LineNo: lineno, Err: err}
			}

			if dec.preserveComments {
				param.Comments = comments
				comments = nil
			}

			seqNum++

			param.Location = Location{FileName: fileName, LineNo: lineno, SeqNum: seqNum}
			sect.Parameters = append(sect.Parameters, param)
		}
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	// Collect non-empty section.
	if len(sect.Parameters) > 0 || sect.Filter != "" {
		newCfg.Sections = append(newCfg.Sections, sect)
	}

	if dec.preserveComments {
		newCfg.Comments = comments
	}

	*cfg = newCfg

	return nil
}

// ParseError describes a parser error.
type ParseError struct {
	LineNo int
	Err    error
}

// Error implements error.
func (pe *ParseError) Error() string {
	return fmt.Sprintf("line %d: %s", pe.LineNo, pe.Err)
}

// Unwrap returns the error that caused parsing to fail.
func (pe *ParseError) Unwrap() error {
	return pe.Err
}

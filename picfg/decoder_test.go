// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package picfg_test

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/picfg"
)

func TestDecoder_Decode(t *testing.T) {
	var cfg picfg.ConfigTxt

	// comments are ignored
	dec := picfg.NewDecoder(bytes.NewBufferString("# comment"))
	testutil.Ok(t, dec.Decode(&cfg))
	testutil.ObjectEqual(t, &cfg, &picfg.ConfigTxt{})

	// lines with only white-space are ignored
	dec = picfg.NewDecoder(bytes.NewBufferString("  \t \n"))
	testutil.Ok(t, dec.Decode(&cfg))
	testutil.ObjectEqual(t, &cfg, &picfg.ConfigTxt{})

	// more complex example
	buf := bytes.NewBufferString(`
# comments are ignored and start with #

# empty lines are ignored as well

# sections are places in [square brackets]
[all]

# everything else is a key=value pair
key=value
other=stuff
goes=here
`)
	dec = picfg.NewDecoder(buf)

	testutil.Ok(t, dec.Decode(&cfg))
	testutil.ObjectEqual(t, &cfg, &picfg.ConfigTxt{
		Sections: []picfg.Section{
			{
				Filter: "[all]",
				Parameters: []picfg.Parameter{
					{Name: "key", Value: "value", Location: picfg.Location{LineNo: 10, SeqNum: 2}},
					{Name: "other", Value: "stuff", Location: picfg.Location{LineNo: 11, SeqNum: 3}},
					{Name: "goes", Value: "here", Location: picfg.Location{LineNo: 12, SeqNum: 4}},
				},
				Location: picfg.Location{LineNo: 7, SeqNum: 1},
			},
		},
	})
}

func TestDecoder_DecodePreservingComments(t *testing.T) {
	var cfg picfg.ConfigTxt

	// comments are ignored
	dec := picfg.NewDecoder(bytes.NewBufferString("# comment"))
	dec.PreserveComments()
	testutil.Ok(t, dec.Decode(&cfg))
	testutil.ObjectEqual(t, &cfg, &picfg.ConfigTxt{Comments: []string{"# comment"}})

	// lines with only white-space are ignored
	dec = picfg.NewDecoder(bytes.NewBufferString("  \t \n"))
	dec.PreserveComments()
	testutil.Ok(t, dec.Decode(&cfg))
	testutil.ObjectEqual(t, &cfg, &picfg.ConfigTxt{Comments: []string{"  \t "}})

	// more complex example
	buf := bytes.NewBufferString(`
# comments are ignored and start with #

# empty lines are ignored as well

# sections are places in [square brackets]
[all]

# everything else is a key=value pair
key=value
other=stuff
goes=here
`)
	dec = picfg.NewDecoder(buf)
	dec.PreserveComments()

	testutil.Ok(t, dec.Decode(&cfg))
	testutil.ObjectEqual(t, &cfg, &picfg.ConfigTxt{
		Sections: []picfg.Section{
			{
				Comments: []string{
					"",
					"# comments are ignored and start with #",
					"",
					"# empty lines are ignored as well",
					"",
					"# sections are places in [square brackets]",
				},
				Filter: "[all]",
				Parameters: []picfg.Parameter{
					{Name: "key", Value: "value", Location: picfg.Location{LineNo: 10, SeqNum: 2}, Comments: []string{"", "# everything else is a key=value pair"}},
					{Name: "other", Value: "stuff", Location: picfg.Location{LineNo: 11, SeqNum: 3}},
					{Name: "goes", Value: "here", Location: picfg.Location{LineNo: 12, SeqNum: 4}},
				},
				Location: picfg.Location{LineNo: 7, SeqNum: 1},
			},
		},
	})
}

func TestDecoder_LocationString(t *testing.T) {
	loc := picfg.Location{FileName: "config.txt", LineNo: 5}
	testutil.Equal(t, loc.String(), "config.txt:5")

	// Nothing fancy happens for an empty location.
	loc = picfg.Location{}
	testutil.Equal(t, loc.String(), ":0")
}

func TestDecoder_DecoderRemembersFileName(t *testing.T) {
	d := t.TempDir()
	name := filepath.Join(d, "config.txt")
	testutil.Ok(t, os.WriteFile(name, []byte("kernel=linux.img"), 0o600))

	f, err := os.Open(name)
	testutil.Ok(t, err)

	defer func() {
		_ = f.Close()
	}()

	dec := picfg.NewDecoder(f)

	var cfg picfg.ConfigTxt

	testutil.Ok(t, dec.Decode(&cfg))

	testutil.SliceHasLen(t, cfg.Sections, 1)
	testutil.SliceHasLen(t, cfg.Sections[0].Parameters, 1)
	testutil.Equal(t, cfg.Sections[0].Parameters[0].Location.FileName, name)
}

func TestDecoder_DecodeErrors(t *testing.T) {
	var cfg picfg.ConfigTxt

	dec := picfg.NewDecoder(bytes.NewBufferString("key"))
	testutil.ErrorMatches(t, dec.Decode(&cfg), `line 1: cannot parse parameter out of "key"`)

	dec = picfg.NewDecoder(bytes.NewBufferString(fmt.Sprintf("key=%98s", "value")))
	testutil.ErrorMatches(t, dec.Decode(&cfg), `line 1: cannot parse line longer than 98 bytes`)

	dec = picfg.NewDecoder(bytes.NewBufferString(fmt.Sprintf("#%98s", "comment")))
	testutil.Ok(t, dec.Decode(&cfg))

	dec = picfg.NewDecoder(bytes.NewBufferString("[all"))
	testutil.ErrorMatches(t, dec.Decode(&cfg), `line 1: cannot parse parameter out of "\[all"`)

	dec = picfg.NewDecoder(bytes.NewBufferString("\nkey"))
	err := dec.Decode(&cfg)
	testutil.ErrorMatches(t, err, `line 2: cannot parse parameter out of "key"`)
	paramErr := testutil.ErrorAs[*picfg.ParameterSyntaxError](t, err)
	testutil.SlicesEqual(t, paramErr.Text, []byte(`key`))
}

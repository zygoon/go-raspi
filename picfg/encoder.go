// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package picfg

import (
	"fmt"
	"io"
)

// Encoder encodes and writes config.txt from an output stream.
type Encoder struct {
	w io.Writer
}

// NewEncoder returns an encoder that writes to w.
func NewEncoder(w io.Writer) *Encoder {
	return &Encoder{w: w}
}

// Encode writes a representation of cfg to the stream.
func (enc *Encoder) Encode(cfg ConfigTxt) error {
	for _, comment := range cfg.Comments {
		if _, err := fmt.Fprintln(enc.w, comment); err != nil {
			return err
		}
	}

	for _, sect := range cfg.Sections {
		for _, comment := range sect.Comments {
			if _, err := fmt.Fprintln(enc.w, comment); err != nil {
				return err
			}
		}

		if sect.Filter != "" {
			if _, err := fmt.Fprintln(enc.w, sect.Filter); err != nil {
				return err
			}
		}

		for _, param := range sect.Parameters {
			for _, comment := range param.Comments {
				if _, err := fmt.Fprintln(enc.w, comment); err != nil {
					return err
				}
			}

			if _, err := fmt.Fprintln(enc.w, param); err != nil {
				return err
			}
		}
	}

	return nil
}

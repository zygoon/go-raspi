// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package picfg models configuration files used by the Raspberry Pi bootloader
//
// Implementation is based on the official documentation
// https://www.raspberrypi.org/documentation/configuration/config-txt/
//
// A tl;dr; variant of the documentation is:
//
//	INI-style [section] key=value file with one exception when the key is
//	"initramfs" then the value separator is " " instead of "=". Sections
//	are stateful but this does not affect parsing.
package picfg

import (
	"bytes"
	"fmt"
	"slices"
)

// ConfigTxt represents the contents of a config.txt file.
//
// The file is represented as a slice of sections. Each section starts with an
// optional conditional filter and is followed by a slice of parameters.
//
// The representation can optionally preserve and restore comments. This makes
// round-trips less jarring, as comments are preserved and mostly retain their
// locations even when edited programmatically.
type ConfigTxt struct {
	Comments []string // Comments or white-space lines without trailing newlines.
	Sections []Section
}

// Equal returns true if two configuration files are identical.
//
// Equal checks that all the sections define the same parameters, in the same
// order and on the same line number. Equal also compares comments, even though
// if they have no semantic value.
func (cfg *ConfigTxt) Equal(other *ConfigTxt) bool {
	return cfg == other || (slices.Equal(cfg.Comments, other.Comments) &&
		objectSlicesEqualPtr(cfg.Sections, other.Sections))
}

// UnmarshalText implements TextUnmarshaler.
//
// Parsed comments discarded. Create a new decoder and use PreserveComments
// before decoding to preserve them.
func (cfg *ConfigTxt) UnmarshalText(text []byte) error {
	return NewDecoder(bytes.NewBuffer(text)).Decode(cfg)
}

// MarshalText implements TextMarshaler.
//
// Comments are marshalled if present.
func (cfg ConfigTxt) MarshalText() ([]byte, error) {
	var buf bytes.Buffer
	if err := NewEncoder(&buf).Encode(cfg); err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

// Location pairs file name with a line number.
//
// The decoder provides location for each section and parameter.
// The encoder ignores those values.
type Location struct {
	FileName string
	LineNo   int

	// SeqNum is a sequence number, greater than zero and monotonically
	// increasing as the decoder reads files. Unlike LineNo, when an "include"
	// directive is read, the sequence number keeps increasing.
	//
	// Sequence number can be used to determine which of a pair of
	// parameters was read earlier.
	SeqNum uint
}

// String returns the location as a file-name:line-number string.
func (loc *Location) String() string {
	return fmt.Sprintf("%s:%d", loc.FileName, loc.LineNo)
}

// Section scopes parameters to an optional conditional filter.
//
// The way a conditional filter impacts boot selection is stateful and depends
// on the filters in prior sections. To understand the effective set of filters
// that apply, use the StateTracker from the condfilter package. The parser
// does not offer this functionality.
type Section struct {
	Comments   []string // Comments or white-space lines without trailing newlines.
	Filter     string   // Filter is either empty or matches /\[^]+\]/.
	Parameters []Parameter
	Location   Location
}

// Equal returns true if the two sections have the same parameters, comment,
// location and any optional conditional filter.
func (sect *Section) Equal(other *Section) bool {
	return sect.Filter == other.Filter &&
		sect.Location == other.Location &&
		slices.Equal(sect.Comments, other.Comments) &&
		objectSlicesEqualPtr(sect.Parameters, other.Parameters)
}

// Parameter represents a single configurable entry.
//
// Most parameters are represented as a key=value pair. Some keys can be assigned
// to multiple times for a stacking effect. This is not represented in the
// parser as the inner semantics of the firmware is not precisely documented.
//
// As an exception, the "initramfs" key is not using "key=value" syntax and
// instead is using "key value", with a space instead of the equals sign.
type Parameter struct {
	Comments []string // Comments or white-space lines without trailing newlines.
	Name     string
	Value    string
	Location Location
}

// Equal returns true if the two paramters have the same comments, name, value
// and location.
func (p *Parameter) Equal(other *Parameter) bool {
	return p.Name == other.Name &&
		p.Value == other.Value &&
		p.Location == other.Location &&
		slices.Equal(p.Comments, other.Comments)
}

// MarshalText implements TextMarshaler.
//
// Comments do not undergo marshaling.
func (p Parameter) MarshalText() ([]byte, error) {
	return []byte(p.String()), nil
}

// UnmarshalText implements TextUnmarshaler.
//
// Comments do not undergo marshaling.
func (p *Parameter) UnmarshalText(text []byte) error {
	var pair [][]byte
	if bytes.HasPrefix(text, []byte(`initramfs `)) {
		pair = bytes.SplitN(text, []byte(" "), 2)
	} else {
		pair = bytes.SplitN(text, []byte("="), 2)
	}

	if len(pair) != 2 {
		return &ParameterSyntaxError{Text: text}
	}

	p.Name = string(pair[0])
	p.Value = string(pair[1])

	return nil
}

func (p Parameter) String() string {
	return fmt.Sprintf("%s%c%s", p.Name, p.glue(), p.Value)
}

// glue returns '=' for all parameters except for `initramfs`, where it returns ' '.
func (p *Parameter) glue() rune {
	if p.Name == "initramfs" {
		return ' '
	}

	return '='
}

// ParameterSyntaxError reports inability to parse a key-value parameter.
type ParameterSyntaxError struct {
	Text []byte
}

// Error implements the error interface.
func (err *ParameterSyntaxError) Error() string {
	return fmt.Sprintf("cannot parse parameter out of %q", string(err.Text))
}

// pointerEquatable wraps the Equal method with a pointer receiver and argument.
type pointerEquatable[T any] interface {
	*T
	Equal(*T) bool
}

// objectSlicesEqualPtr returns true if two slices have have equal objects and
// the equal method uses pointer receiver.
func objectSlicesEqualPtr[T any, ptrT pointerEquatable[T]](a, b []T) bool {
	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if !ptrT(&a[i]).Equal(ptrT(&b[i])) {
			return false
		}
	}

	return true
}

// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package picfg_test

import (
	"bytes"
	"testing"

	"gitlab.com/zygoon/go-raspi/internal/testutil"
	"gitlab.com/zygoon/go-raspi/picfg"
)

func TestEncoder_Encode(t *testing.T) {
	var buf bytes.Buffer
	enc := picfg.NewEncoder(&buf)
	err := enc.Encode(picfg.ConfigTxt{
		Sections: []picfg.Section{
			{
				Parameters: []picfg.Parameter{
					{Name: "key", Value: "value"},
				},
			},
			{
				Filter: "[pi4]",
				Parameters: []picfg.Parameter{
					{Name: "initramfs", Value: "tricky"},
					{Name: "unused", Value: "stuff"},
				},
			},
		},
	})
	testutil.Ok(t, err)
	testutil.Equal(t, buf.String(), ""+
		"key=value\n"+
		"[pi4]\n"+
		"initramfs tricky\n"+
		"unused=stuff\n")
}

func TestEncoder_EncodeComments(t *testing.T) {
	var buf bytes.Buffer
	enc := picfg.NewEncoder(&buf)
	err := enc.Encode(picfg.ConfigTxt{
		Sections: []picfg.Section{
			{
				Comments: []string{
					"",
					"# comments are ignored and start with #",
					"",
					"# empty lines are ignored as well",
					"",
					"# sections are places in [square brackets]",
				},
				Filter: "[all]",
				Parameters: []picfg.Parameter{
					{Name: "key", Value: "value", Location: picfg.Location{LineNo: 10, SeqNum: 2}, Comments: []string{"", "# everything else is a key=value pair"}},
					{Name: "other", Value: "stuff", Location: picfg.Location{LineNo: 11, SeqNum: 3}},
					{Name: "goes", Value: "here", Location: picfg.Location{LineNo: 12, SeqNum: 4}},
				},
				Location: picfg.Location{LineNo: 7, SeqNum: 1},
			},
		},
	})
	testutil.Ok(t, err)
	testutil.Equal(t, buf.String(), `
# comments are ignored and start with #

# empty lines are ignored as well

# sections are places in [square brackets]
[all]

# everything else is a key=value pair
key=value
other=stuff
goes=here
`)
}

func TestEncoder_EncodeFileComments(t *testing.T) {
	var buf bytes.Buffer
	enc := picfg.NewEncoder(&buf)
	err := enc.Encode(picfg.ConfigTxt{
		Comments: []string{"# Hello"},
	})
	testutil.Ok(t, err)
	testutil.Equal(t, buf.String(), "# Hello\n")
}
